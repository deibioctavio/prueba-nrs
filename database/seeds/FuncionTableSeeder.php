<?php

use Illuminate\Database\Seeder;
use App\Sala;
use App\Pelicula;

class FuncionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salas = Sala::orderBy('nombre','ASC')->get();
        $peliculas = Pelicula::orderBy('nombre','ASC')->get();
        $semana = explode ('|', getenv('SEMANA'));

        foreach ( $salas as $i => $s ) {
            
            foreach ( $peliculas as $i => $p ) {

                foreach( $semana as $dia ){
                    
                    for( $f = getenv('HORA_INICIAL_FUNCIONES'); $f <= getenv('HORA_FINAL_FUNCIONES'); $f+=2){

                        $ff = $f+2;
                        $funcion = [
                            'nombre' => $p->nombre."| ".$f." - ".((int)($f+2))." | $dia | $s->nombre",
                            'fecha_inicio' => date('Y-m-d')." $f:00:00",
                            'fecha_fin' => date('Y-m-d')." $ff:00:00",
                            'pelicula_id' => $p->id,
                            'sala_id' => $s->id,
                        ];
    
                        DB::table('funciones')->insert($funcion);
                    }
                }
            }
        }
    }
}
