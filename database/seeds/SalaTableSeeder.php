<?php

use Illuminate\Database\Seeder;
use App\Empresa;

class SalaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresas = Empresa::orderBy('nombre','ASC')->get();

        foreach ( $empresas as $i => $r ) {
            
            $cantidadSalas = rand(getenv('NUMERO_MINIMO_SALAS'),getenv('NUMERO_MAXIMO_SALAS'));

            for ( $j = 0 ; $j < $cantidadSalas; $j++ ){

                $sala = [
                    'nombre' => 'Sala #'.((int)($j+1)),
                    'observaciones' => 'Sala #'.((int)($j+1)).' - '.$r->nombre,
                    'empresa_id' => $r->id,
                ];

                DB::table('salas')->insert($sala);
            }                       
        }
    }
}
