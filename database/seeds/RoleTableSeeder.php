<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use Bican\Roles\Models\Role;

class RoleTableSeeder extends Seeder {
	public function run() {

		DB::table('role_user')->delete();
		DB::table('roles')->delete();

		DB::table('roles')->insert(
			[
				[
					'id' => 1,
					'name' => 'Administrador',
				    'slug' => 'admin',
				    'description' => 'Administrador aplicación', // optional
				    'level' => 1, // optional, set to 1 by default
				    'created_at' => time(),
				],
				[
					'id' => 2,
					'name' => 'Cliente',
				    'slug' => 'cliente',
				    'description' => 'Cliente', // optional
				    'level' => 2, // optional, set to 1 by default
				    'created_at' => time(),
				]
			]
		);

		$role = Role::where('slug', 'admin')->first();
		$user = User::where('name', 'usuarioadmin')->first();
		if($role && $user){
		    $user->attachRole($role);
		}
	}
}
?>
