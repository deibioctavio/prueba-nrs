<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProfileTableSeeder extends Seeder {

	public function run() {

		DB::table('profiles')->delete();

        DB::table('profiles')->insert([
            'name' => 'Usuario Administrador',
            'lastname' => 'Todos Los Provilegios',
            'document_type' => 'CC',
            'document_number' => '110001100011100',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 1,
        ]);

        /*
        DB::table('profiles')->insert([
            'name' => 'Jorge Leonardo',
            'lastname' => 'Muñoz',
            'document_type' => 'CC',
            'document_number' => '220002202022200',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 2,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Tecnico 1',
            'lastname' => 'Apellido',
            'document_type' => 'CC',
            'document_number' => '330003303033300',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 3,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Ingeniero 1',
            'lastname' => 'Apellido',
            'document_type' => 'CC',
            'document_number' => '440004404044400',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 4,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Gerente 1',
            'lastname' => 'Apellido',
            'document_type' => 'CC',
            'document_number' => '550005505055500',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 5,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Generador 1',
            'lastname' => 'Externo',
            'document_type' => 'CC',
            'document_number' => '660006606066600',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 6,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Administrador 1',
            'lastname' => 'Apellidos',
            'document_type' => 'CC',
            'document_number' => '770007707077700',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 7,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Gerente 2',
            'lastname' => 'Apellido',
            'document_type' => 'CC',
            'document_number' => '880008808088800',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 8,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Tecnico 2',
            'lastname' => 'Apellido',
            'document_type' => 'CC',
            'document_number' => '99000990909900',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 9,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Tecnico 3',
            'lastname' => 'Apellido',
            'document_type' => 'CC',
            'document_number' => '10101010101010',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 10,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);

				DB::table('profiles')->insert([
            'name' => 'Ingeniero 2',
            'lastname' => 'Apellido',
            'document_type' => 'CC',
            'document_number' => '11111111111111',
            'birthday' => date('Y-m-d H:i:s'),
            'address' => 'Sin dirección',
            'gender' => 'M',
            'phone' => '(+57)(6) 7000000',
            'user_id' => 11,
						'empresa_id' => env('DEFAULT_SUCURSAL_ID')
        ]);
        */
	}
}
?>
