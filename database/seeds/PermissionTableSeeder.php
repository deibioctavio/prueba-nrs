<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('permission_role')->delete();
        DB::table('permissions')->delete();
        
        /*
        * Areas/Dependencias Institución
        */

        DB::table('permissions')->insert([
            'name' => 'Adicionar Ubicación',
            'slug' => 'ubicacion.add',
            'description' => 'Permiso de Adicionar Ubicación (Maker Google Maps)',
            'group' => 'ubicacion',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Editar Ubicación',
            'slug' => 'ubicacion.edit',
            'description' => 'Permiso de Editar Ubicación',
            'group' => 'ubicacion',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Eliminar Ubicación',
            'slug' => 'ubicacion.delete',
            'description' => 'Permiso de Eliminar Ubicación',
            'group' => 'ubicacion',
        ]);

        DB::table('permissions')->insert([
            'name' => 'Ver Ubicación',
            'slug' => 'ubicacion.show',
            'description' => 'Permiso de Ver Ubicación',
            'group' => 'ubicacion',
        ]);

         DB::table('permissions')->insert([
            'name' => 'Ver Todas las Ubicaciones',
            'slug' => 'ubicacion.showall',
            'description' => 'Permiso de Ver Todas las Ubicaciones',
            'group' => 'ubicacion',
        ]);

        $role = Role::where('slug', 'admin')->first();
        $permissions = Permission::get();

        if($role && $permissions){

            foreach ($permissions as $p) {
                $role->attachPermission($p);
            }
        }
    }
}
