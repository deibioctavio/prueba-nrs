<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    // php artisan db:seed
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(PermissionRouteTableSeeder::class);
        $this->call(EmpresaTableSeeder::class);
        $this->call(ProfileTableSeeder::class);
        $this->call(PeliculaTableSeeder::class);
        $this->call(SalaTableSeeder::class);
        $this->call(FuncionTableSeeder::class);

        Model::reguard();
    }
}
