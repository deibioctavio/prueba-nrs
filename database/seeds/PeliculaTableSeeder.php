<?php

use Illuminate\Database\Seeder;

class PeliculaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('peliculas')->delete();
        
        DB::table('peliculas')->insert(
                                        [
                                        'nombre' => 'Batman: The Dark Knight',
                                        'sinopsis' => 'When the menace known as the Joker emerges from his mysterious past, he wreaks havoc and chaos on the people of Gotham. The Dark Knight must accept one of the greatest psychological and physical tests of his ability to fight injustice.',
                                        'director' => 'Christopher Nolan',
                                        'idioma' => 'English',
                                        'clasificacion' => 'PG-13',
                                        'duracion_minutos' => '152',
                                        'ano' => '2008',
                                        'fecha_salida' => '2008-07-18',
                                        ]
                                    );
        
        DB::table('peliculas')->insert(
            [
            'nombre' => 'Deadpool',
            'sinopsis' => 'A fast-talking mercenary with a morbid sense of humor is subjected to a rogue experiment that leaves him with accelerated healing powers and a quest for revenge.',
            'director' => 'Tim Miller',
            'idioma' => 'English',
            'clasificacion' => 'R',
            'duracion_minutos' => '108',
            'ano' => '2016',
            'fecha_salida' => '2016-02-12',
            ]
        );

        DB::table('peliculas')->insert(
            [
            'nombre' => 'Guardians of the Galaxy',
            'sinopsis' => 'A group of intergalactic criminals are forced to work together to stop a fanatical warrior from taking control of the universe.',
            'director' => 'James Gunn',
            'idioma' => 'English',
            'clasificacion' => 'PG-13',
            'duracion_minutos' => '121',
            'ano' => '2014',
            'fecha_salida' => '2014-08-01',
            ]
        );

        DB::table('peliculas')->insert(
            [
            'nombre' => 'It',
            'sinopsis' => 'It is a movie starring Bill Skarsgård, Jaeden Lieberher, and Finn Wolfhard. In the summer of 1989, a group of bullied kids band together to destroy a shapeshifting monster, which disguises itself as a clown and preys on the children of Derry, their small Maine town.',
            'director' => 'Andy Muschietti',
            'idioma' => 'English',
            'clasificacion' => 'R',
            'duracion_minutos' => '135',
            'ano' => '2017',
            'fecha_salida' => '2017-09-08',
            ]
        );
    }
}
