<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'usuarioadmin',
            'email' => 'deibioctavio@gmail.com',
            'password' => bcrypt('deibioctavio'),
            'created_at' => getTimestamp(),
        ]);

        /*DB::table('users')->insert([
            'id' => 2,
            'name' => 'jorgeleonardo',
            'email' => 'm.r.leon@hotmail.com',
            'password' => bcrypt('m.r.leon'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 3,
            'name' => 'tecnico1',
            'email' => 'tecnico1@empresa.com',
            'password' => bcrypt('tecnico1'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 4,
            'name' => 'ingeniero1',
            'email' => 'ingeniero1@empresa.com',
            'password' => bcrypt('ingeniero1'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 5,
            'name' => 'gerente1',
            'email' => 'gerente1@empresa.com',
            'password' => bcrypt('gerente1'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 6,
            'name' => 'generadorordenexterno',
            'email' => 'generadorordenexterno@empresa.com',
            'password' => bcrypt('generadorordenexterno'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 7,
            'name' => 'administrador',
            'email' => 'administrador@gamebox.com',
            'password' => bcrypt('administrador'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 8,
            'name' => 'gerente2',
            'email' => 'gerente2@gamebox.com',
            'password' => bcrypt('gerente2'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 9,
            'name' => 'tecnico2',
            'email' => 'tecnico2@gamebox.com',
            'password' => bcrypt('tecnico2'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 10,
            'name' => 'tecnico3',
            'email' => 'tecnico3@gamebox.com',
            'password' => bcrypt('tecnico3'),
            'created_at' => getTimestamp(),
        ]);

        DB::table('users')->insert([
            'id' => 11,
            'name' => 'ingeniero2',
            'email' => 'ingeniero2@gamebox.com',
            'password' => bcrypt('ingeniero2'),
            'created_at' => getTimestamp(),
        ]);*/
    }
}
