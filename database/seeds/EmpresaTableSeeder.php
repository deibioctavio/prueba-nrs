<?php

use Illuminate\Database\Seeder;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('empresas')->delete();

      $datosEmpresa = array(
        'id' => 1,
        'nombre' =>'Procinal Av Suba.',
        'direccion' =>'Av Suba. con 127. CC Bulevar Nisa.',
        'telefono' => '(+57) (1) 628 17 65',
        'correo_electronico' => 'salaavsuba@procinal.com.co',
      );

      DB::table('empresas')->insert($datosEmpresa);
      
      $datosEmpresa = array(
        'id' => 2,
        'nombre' =>'Cine Colombia Iserra 100.',
        'direccion' =>'Av Suba. Calle 100 CC Iserra 100.',
        'telefono' => '(+57) (1) 650 12 87',
        'correo_electronico' => 'iserra100@cinecolombia.com.co',
      );

      DB::table('empresas')->insert($datosEmpresa);

      $datosEmpresa = array(
        'id' => 3,
        'nombre' =>'Cine Colombia Unicentro.',
        'direccion' =>'Carrera 17. Calle 127 CC Unicentro Bogotá.',
        'telefono' => '(+57) (1) 761 65 42',
        'correo_electronico' => 'unicentrobogota@cinecolombia.com.co',
      );

      DB::table('empresas')->insert($datosEmpresa);

      $datosEmpresa = array(
        'id' => 4,
        'nombre' =>'Cinemark Gran Estación.',
        'direccion' =>'Calle 26 #89-32 CC La Gran Estación.',
        'telefono' => '(+57) (1) 543 76 72',
        'correo_electronico' => 'lagranestacion@cinemark.com.co',
      );

      DB::table('empresas')->insert($datosEmpresa);
    }
}
