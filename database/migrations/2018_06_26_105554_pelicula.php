<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pelicula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peliculas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nombre',100);
            $table->text('sinopsis');
            $table->string('director',100);
            $table->string('idioma',30);
            $table->text('clasificacion',10);
            $table->integer('duracion_minutos')->unsigned();
            $table->integer('ano')->unsigned();
            $table->dateTime('fecha_salida');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('peliculas');
    }
}
