<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function ($table) {
            $table->integer('valor_hora')->unsigned()->default(0)->after('nombre_foto'); 
            $table->integer('valor_hora_nocturna')->unsigned()->default(0)->after('valor_hora');
            $table->integer('valor_hora_festivo')->unsigned()->default(0)->after('valor_hora_nocturna');
            $table->integer('valor_hora_dominical')->unsigned()->default(0)->after('valor_hora_festivo'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colors', function ($table) {
            $table->dropColumn('valor_hora');
            $table->dropColumn('valor_hora_nocturna');
            $table->dropColumn('valor_hora_festivo');
            $table->dropColumn('valor_hora_dominical');
        });
    }
}
