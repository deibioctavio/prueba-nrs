<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name',50);
            $table->string('lastname',50);
            $table->string('document_type',20);
            $table->string('document_number',50);
            $table->date('birthday')->nullable();
            $table->text('address',255)->nullable();
            $table->enum('gender',['M', 'F','O'])->default('M');
            $table->string('phone',20)->nullable();
            $table->string('cellphone',20)->nullable();
            $table->string('nombre_foto',50)->nullable();
            $table->integer('user_id')->unique()->unsigned()->index();
            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    public function users() {
        return $this->belongsTo('App\User');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
