<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReservaButacas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserva_butacas', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('fila_butaca')->unsigned();
            $table->integer('columna_butaca');
            $table->integer('reserva_id')->unsigned()->index();
            $table->foreign('reserva_id')->references('id')->on('reservas')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reserva_butacas');
    }
}
