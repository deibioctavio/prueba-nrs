<?php
  $_formId = isset($formId)?$formId:0;
  $_uploadControllerRoute = isset($uploadControllerRoute)?$uploadControllerRoute:"addajax";
  $_uploadControllerRouteAlias = isset($uploadControllerRouteAlias)?$uploadControllerRouteAlias:"addentryajax";
  $_uploadLinkName = isset($uploadLinkName)?$uploadLinkName:"Subir Archivo";
  $_hiddemForm = isset($hiddemForm)?$hiddemForm:true;
  $hidenDivText = $_hiddemForm?' class="hidden "':"";
  $_fileEntryContainerId =isset($fileEntryContainerId)?$fileEntryContainerId:"fileentry-container";
?>
<script type="text/javascript">
    $(document).ready(function(){

        var filename<?php echo $_formId?> = "";
        $('#loading-animation<?php echo $_formId?>').hide();
        $('#anchor-link-delete<?php echo $_formId?>').hide();
        $('#anchor-link-download<?php echo $_formId?>').hide();

        $("#ajax-submit<?php echo $_formId?>").click(function(e){
            e.preventDefault();
            $('#filefield<?php echo $_formId?>').trigger("click");
         });

         $("#ajax-download<?php echo $_formId?>").click(function(e){
             e.preventDefault();
             $('#loading-animation<?php echo $_formId?>').show();
             $('#anchor-link-delete<?php echo $_formId?>').hide();
             $('#anchor-link-download<?php echo $_formId?>').hide();
             var itemId = $(this).attr("item-id");

             $.ajax({
                     url:'fileentry/getajax/' + itemId,
                     async:false,
                     type:'get',
                     /*processData: false,
                     contentType: false,*/
                     responseType: 'blob',
                     beforeSend:function(){/*console.log("before");*/},
                     complete:function(){/*console.log("complete");*/},
                     success:function(response,status,request){

                        var cT = request.getResponseHeader('Content-Type');
                        var cD = request.getResponseHeader('Content-Disposition');
                        var regex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                        var filename = regex.exec(cD);

                        download('data:'+cT+';base64,'+response,filename[1],cT);
                        $('#loading-animation<?php echo $_formId?>').hide();
                        $('#anchor-link-delete<?php echo $_formId?>').show();
                        $('#anchor-link-download<?php echo $_formId?>').show();
                        return true;
                     },
                    error: function(xhr, textStatus, errorThrown)
       			        {
       			            alert('error al intentar obtener el archivo');
       			            return false;
       			        }
             });
          });

          $("#ajax-delete<?php echo $_formId?>").click(function(e){

              e.preventDefault();
              $('#loading-animation<?php echo $_formId?>').show();
              $('#anchor-link-delete<?php echo $_formId?>').hide();
              $('#anchor-link-download<?php echo $_formId?>').hide();

              var itemId = $(this).attr("item-id");

              $.ajax({
                      url:'fileentry/deleteajax/' + itemId,
                      async:false,
                      type:'get',
                      /*processData: false,
                      contentType: false,*/
                      //responseType: 'blob',*/
                      beforeSend:function(){/*console.log("before");*/},
                      complete:function(){/*console.log("complete");*/},
                      success:function(response,status,request){
                        //console.log(response);
                        $('input[type=hidden][id=uploaded_file_id<?php echo $_formId?>]').val(0);
                        $('#loading-animation<?php echo $_formId?>').hide();
                        $('#anchor-link<?php echo $_formId?>').show();
                        imageValidFormat<?php echo $_formId?> = false;
                        $('#fileentryid<?php echo $_formId?>').remove()
                        return true;
                      },
                     error: function(xhr, textStatus, errorThrown)
        			        {
        			            alert('error al intentar obtener el archivo');
        			            return false;
        			        }
              });
           });

        $("#filefield<?php echo $_formId?>").click(function(){

            filename<?php echo $_formId?> = $('input[type=file][id=filefield<?php echo $_formId?>]').val();
         });

        $("#filefield<?php echo $_formId?>").on("change",function(){

            console.log('change');
            var extension<?php echo $_formId?> = fileGetExtension($('input[type=file][id=filefield<?php echo $_formId?>]'));

            if( !isImageExtension(extension<?php echo $_formId?>) ){
              alert("Solo son validos archivos de imágenes en formato .PNG, .JPG ó .GIF");
              $("#anchor-link<?php echo $_formId?>").show();
              return false;
            }

            $("#anchor-link<?php echo $_formId?>").hide();
            $('#loading-animation<?php echo $_formId?>').show();

            $.ajax({
                    url:'fileentry/addajax',
                    data:new FormData($("#upload_form<?php echo $_formId?>")[0]),
                    dataType:'json',
                    async:false,
                    type:'post',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){/*console.log("before");*/},
                    complete:function(){/*console.log("complete");*/},
                    success:function(response){
                      $('input[type=hidden][id=uploaded_file_id<?php echo $_formId?>]').val(response.filedata.id);
                      $('#loading-animation<?php echo $_formId?>').hide();
                      $('#ajax-delete<?php echo $_formId?>').attr('item-id',response.filedata.id)
                      $('#ajax-download<?php echo $_formId?>').attr('item-id',response.filedata.id);
                      $('#anchor-link-download<?php echo $_formId?>').show();
                      $('#anchor-link-delete<?php echo $_formId?>').show();
                      var fileentryInput = '<input type="hidden" name="fileentryid[]" ';
                      fileentryInput += 'id="fileentryid<?php echo $_formId?>" ';
                      fileentryInput += 'filename="'+response.filedata.filename+'" ';
                      fileentryInput += 'value="'+response.filedata.id+'" />';
                      $('#<?php echo $_fileEntryContainerId;?>').append(fileentryInput);
                    },
                    error: function(xhr, textStatus, errorThrown)
      			        {
      			            alert('error al intertar subir el archivo');
      			            return false;
      			        }
            });
        });
    });
</script>
<div id="UploadFormDiv<?php echo $_formId?>" <?php echo $hidenDivText?>>
  <form
        action="{{route('<?php echo $_uploadControllerRouteAlias?>', [])}}"
        method="post" enctype="multipart/form-data"
        id="upload_form<?php echo $_formId?>"
  >
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <input type="hidden" name="element_id" value="<?php echo $_formId?>" />
    <input type="file" name="filefield<?php echo $_formId?>" id="filefield<?php echo $_formId?>">
  </form>
</div>
<div class="row-fluid strech">
    <div id="anchor-link<?php echo $_formId?>">
      <a href="#" id="ajax-submit<?php echo $_formId?>"><?php echo $_uploadLinkName?></a>
    </div>
    <div class="loading-animation" id="loading-animation<?php echo $_formId?>">
      <img src="{{ asset('assets/images/loading.gif') }}" height="16" width="16" />
    </div>
    <div id="anchor-link-download<?php echo $_formId?>">
      <a href="#" id="ajax-download<?php echo $_formId?>" item-id="0">Descargar&nbsp;|&nbsp;</a></div>
    <div id="anchor-link-delete<?php echo $_formId?>">
      <a href="#" id="ajax-delete<?php echo $_formId?>" item-id="0">Eliminar</a>
    </div>
    <input type="hidden" name="uploaded_file_id[]" id="uploaded_file_id<?php echo $_formId?>" value="0">
    <br class="clear" />
</div>
