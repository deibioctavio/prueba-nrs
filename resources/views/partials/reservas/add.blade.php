<script type="text/javascript">
	var rowsCount = 0;
	var _limg = jsAjaxLoadingImageObjectString16;


	$(document).ready(function(){

		var r = null;

		$("#register-frm").validate({
	        rules: {
	            descripcion: { required: true},
							horas: {
												required: true,
												number: true,
												min: 0,
											},
	        },
	        messages: {

	            descripcion: {
	                required: "Ingrese la descripción de la Orden de Trabajo"
	            },
							horas: {
	                required: "Ingrese el número de horas dedicadas",
									number: "El valor ingresado debe ser un número",
									min: "Debe ingresar un valor mayor a cero (0)"
	            },
	        },

	        submitHandler: function(form) {
	            form.submit();
	        }
		});

		$('#pelicula_id').on('change',function(){
			getFuncionesByPeliculaIdAndSalaId();
		});

		$('#teatro_id').on('change',function(){
			getSalaByTeatro();
		});

		getSalaByTeatro();
	});

	function hideLoadingTr(){
		$('tr[id=loading-tr]').remove();
	}

	function showLoadingTr(){
		var l = '<tr id="loading-tr"><td colspan="8">'+jsAjaxLoadingImageObjectString32+'</td></tr>';
		$('table.table tbody').append(l);
	}

	function getSalaByTeatro(){

		$.ajax(
				{
						url: base_url + "/getsalabyteatroid/"+$('#teatro_id').val(),
						data:{},
						dataType:'json',
						//async:false,
						//processData: true,
						//contentType: 'json',
						type:'GET',
						beforeSend: function(){
							$('#salas-container').html(jsAjaxLoadingImageObjectString24);
						},
						success: function(response)
						{
							$('select[id=sala_id]').unbind('change');
							var select = '<select id="sala_id" name="sala_id">';

							$.each(response, function( index, element ) {
								select += '<option value="'+element.id+'">'+element.nombre+'</option>';
							});

							select += '</select>';

							$('#salas-container').html(select);

							$('select[id=sala_id]').on('change',function(){
								getFuncionesByPeliculaIdAndSalaId();
							});

							getFuncionesByPeliculaIdAndSalaId();
						},
						error: function(xhr, textStatus, errorThrown)
						{
								alert('error al intentar cargar los datos');
								return false;
						}
				});
		}

		function getFuncionesByPeliculaIdAndSalaId(){

			$.ajax(
					{
							url: base_url + '/getfuncionesbypeliculaidsalaid',
							data:{
								pelicula_id: $('#pelicula_id').val(),
								sala_id: $('#sala_id').val(),
								_token: $('input[name=_token]').val()
							},
							dataType:'json',
							//async:false,
							//processData: true,
							//contentType: 'json',
							type:'POST',
							beforeSend: function(){
								$('#horarios-container').html(jsAjaxLoadingImageObjectString24);
							},
							success: function(response)
							{								
								$('select[id=funcion_id]').unbind('change');
								var select = '<select id="funcion_id" name="funcion_id">';

								$.each(response, function( index, element ) {
									select += '<option value="'+element.id+'">'+element.nombre+'</option>';
								});

								select += '</select>';

								$('#horarios-container').html(select);
								setFunctionLabel();

								$('select[id=funcion_id]').on('change',function(){
									setFunctionLabel();
								});
								
							},
							error: function(xhr, textStatus, errorThrown)
							{
									alert('error al intentar cargar los datos');
									return false;
							}
					});
			}

			function setFunctionLabel(){
				var text = $( "#funcion_id option:selected" ).text();
				$('#funcion-container').html(text);

				getButacasByFuncionId();
			}

			function getReservasByFuncionId(){

				$.ajax(
						{
								url: base_url + '/getreservasbyfuncion',
								data:{
									funcion_id: $('#funcion_id').val(),
									_token: $('input[name=_token]').val()
								},
								dataType:'json',
								//async:false,
								//processData: true,
								//contentType: 'json',
								type:'POST',
								beforeSend: function(){
									$('#horarios-container').html(jsAjaxLoadingImageObjectString24);
								},
								success: function(response)
								{								
									$('select[id=funcion_id]').unbind('change');
									var select = '<select id="funcion_id" name="funcion_id">';
	
									$.each(response, function( index, element ) {
										select += '<option value="'+element.id+'">'+element.nombre+'</option>';
									});
	
									select += '</select>';
	
									$('#horarios-container').html(select);
	
									
								},
								error: function(xhr, textStatus, errorThrown)
								{
										alert('error al intentar cargar los datos');
										return false;
								}
						});
				}

				function getButacasByFuncionId(){

					$.ajax(
							{
									url: base_url + '/getbutacasbyfuncionid/' + $('#funcion_id').val(),
									data:{},
									dataType:'json',
									//async:false,
									//processData: true,
									//contentType: 'json',
									type:'get',
									beforeSend: function(){
										$('div[id^=butacas-container-]').each(
											function(){
												$(this).hide()
											}
										);

										$('input[type=checkbox][id^=bt_]').prop('disabled',false);
										$('input[type=checkbox][id^=bt_]').prop('checked', false);
										$('div[id^=bt_]').removeClass('butaca-fill');
										$('div[id^=bt_]').removeClass('butaca-fill-other');
										$('div[id^=bt_]').addClass('butaca');
									},
									success: function(response)
									{	
										console.log(response);
										$.each(response, function( index, element ) {
											
											$('input[type=checkbox][id=bt_'+element.fila_butaca+'_'+element.columna_butaca+']').prop('disabled',true);
											$('input[type=checkbox][id=bt_'+element.fila_butaca+'_'+element.columna_butaca+']').prop('checked', true);
											$('div[id=bt_'+element.fila_butaca+'_'+element.columna_butaca+']').removeClass('butaca');
											$('div[id=bt_'+element.fila_butaca+'_'+element.columna_butaca+']').addClass('butaca-fill');

										});

										getOtrasButacasByFuncionId();
		
										$('div[id^=butacas-container-]').each(
											function(){
												$(this).show()
											}
										);
									},
									error: function(xhr, textStatus, errorThrown)
									{
											alert('error al intentar cargar los datos');
											return false;
									}
							});
					}

					function getOtrasButacasByFuncionId(){

						$.ajax(
								{
										url: base_url + '/getotrasbutacasbyfuncionid/' + $('#funcion_id').val(),
										data:{},
										dataType:'json',
										//async:false,
										//processData: true,
										//contentType: 'json',
										type:'get',
										beforeSend: function(){
											$('div[id^=butacas-container-]').each(
												function(){
													$(this).hide()
												}
											);
										},
										success: function(response)
										{									
											console.log(response);
											
											$.each(response, function( index, element ) {
												
												$('input[type=checkbox][id=bt_'+element.fila_butaca+'_'+element.columna_butaca+']').prop('disabled',true);
												$('input[type=checkbox][id=bt_'+element.fila_butaca+'_'+element.columna_butaca+']').prop('checked', true);
												$('div[id=bt_'+element.fila_butaca+'_'+element.columna_butaca+']').removeClass('butaca');
												$('div[id=bt_'+element.fila_butaca+'_'+element.columna_butaca+']').addClass('butaca-fill-other');
	
											});
			
											$('div[id^=butacas-container-]').each(
												function(){
													$(this).show()
												}
											);
										},
										error: function(xhr, textStatus, errorThrown)
										{
												alert('error al intentar cargar los datos');
												return false;
										}
								});
						}
</script>
