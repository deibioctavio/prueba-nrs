<script type="text/javascript">
	$(document).ready(function(){

		var r = null;

		$("#register-frm").validate({
	        rules: {
	            nombre: { required: true},
							marca: { required: true},
							identificador: { required: true},
							consecutivo: {
															required: true,
															number: true,
															min: 0,
													},
	        },
	        messages: {

	            nombre: {
	                required: "Ingrese el Nombre de la Maquina o Activo"
	            },
							marca: {
	                required: "Ingrese la Marca de la Maquina o Activo"
	            },
							identificador: {
	                required: "Ingrese el Serial/Identificador (ID) de la Maquina/Activo"
	            },
							consecutivo: {
	                required: "Ingrese el valor inicial del consecutivo",
									number: "El valor ingresado debe ser un número",
									min: "Debe ingresar un valor mayor a cero (0)"
	            },
	        },

	        submitHandler: function(form) {
	            form.submit();
	        }
		});

		$('#categoria_maquina_id').on('change',function(){
				getSubcategoriaMaquinaByCategoria('categoria_maquina_id','subcategoria-container');
		});

		//getSubcategoriaMaquinaByCategoria('categoria_maquina_id','subcategoria-container');
	});

function getSubcategoriaMaquinaByCategoria( elementId, containerId ){

	$.ajax(
			{
					url: base_url + '/subcategoriagetbycategoriaidajax',
					data:{
									id: $('#categoria_maquina_id').val(),
									_token: $('input[name=_token]').val()
								},
					dataType:'json',
					//async:false,
					//processData: true,
					//contentType: 'json',
					type:'POST',
					beforeSend: function(){
						$('#subcategoria-container').html(jsAjaxLoadingImageObjectString24);
						$('input[type=submit][value=Registrar]').hide();
					},
					success: function(response)
					{
						console.log(response);
						r = response;
						var select = '<select id="subcategoria_maquina_id" name="subcategoria_maquina_id">';

						$.each(response, function( index, element ) {
							select += '<option value="'+element.id+'">'+element.nombre+'</option>';
						});

						select += '</select>';

						$('#subcategoria-container').html(select);
						$('input[type=submit][value=Registrar]').show();
						return true;

					},
					error: function(xhr, textStatus, errorThrown)
					{
							alert('error al intentar cargar los datos');
							return false;
					}
			});
		}
</script>
