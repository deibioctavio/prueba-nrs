<script type="text/javascript">
	$(document).ready(function(){

		var v = $("#register-frm").validate({
	        rules: {
	            valor: {
												required: true,
												number: true,
												min: 0

											},
							cantidad: {
													required: true,
													number: true,
													min: 1
												}
	        },
	        messages: {

	            valor: {
	                required: "Ingrese el valor unitario del repuesto",
									number: "Debe ingresar un valor numérico",
									min: "Debe ingresar un número positivo"
	            },
							cantidad: {
	                required: "Ingrese la cantidad de elementos a adicionar al inventario",
									number: "Debe ingresar un valor numérico",
									min: "Debe adicionar almenos un nuevo elemento"
	            }
	        },

	        submitHandler: function(form) {
	            //form.submit();
							$("input[type=submit][id=modal-modifcacion-save]").trigger('click');
	        }
		});

    $('a[id=modal-modifcacion-trigger]').trigger('click');
    $('a[id=modal-modifcacion-trigger]').hide();
		$('div[id=modal-body-loading]').html(jsAjaxLoadingImageObjectString32);
		$('div[id=modal-body-loading]').hide();
		
		$('button[id=modal-modifcacion-cancel]').on('click',function(){
			showAllModificarAnchors();
		});

		$("input[type=submit][id=modal-modifcacion-save]").on('click',function(){
			show_confirm_dialog();
		});
  });


	function show_confirm_dialog(){

		$("input[type=submit][id=modal-modifcacion-save]").confirm({
				title:"Confirmar Modificación",
				text: "Este procedimiento es irreversible y no se puede deshacer. Los datos modificados no se podrán recuperar. ¿Está seguro que querer realizarlo?",
				confirm: function(button) {
					 $('div[id=modal-body-loading]').show();
					 $('div[id=modal-body-content]').hide();
					 modificar_existencia_inventario();
				},
				cancel: function(button) {
						button.fadeOut(500).fadeIn(500);
				},
				confirmButton: "Si, Modificar",
				cancelButton: "No, Cancelar Modificación"
		});
	}

	function modificar_existencia_inventario(){

		$.ajax({

			url: base_url + '/repuestoinventarioeditbyidajax',
			data:{
							id: {{$inventario->id}},
							_token: $('input[name=_token]').val(),
							tipo_evento: 'ingreso',
							cantidad: $('input[type=text][name=cantidad]').val(),
							valor: $('input[type=text][name=valor]').val(),
						},
			cache: false,
			dataType:'html',
			//async:false,
			//processData: false,
			//contentType: 'html',
			type:'POST',
			beforeSend: function(){
				$('input[type=submit][id=modal-modifcacion-save]').attr('disabled',true);
				$('button[id=modal-modifcacion-cancel]').attr('disabled',true);
			},
			success: function(response)
			{
				$('button[id=modal-modifcacion-cancel]').attr('disabled',false);
				$('button[id=modal-modifcacion-cancel]').trigger('click');
				$('input[type=button][value=Buscar]').trigger('click');
				showAllModificarAnchors();
			},

			error: function(xhr, textStatus, errorThrown)
			{
					alert('error al intentar modificar los datos');
					return false;
			}
		});
	}
</script>
<a
    id="modal-modifcacion-trigger"
    href="#"
    class="btn btn-info btn-lg"
    data-toggle="modal"
    data-target="#modal-modifcacion"
		data-backdrop="static"
		data-keyboard="false"
>Modal</a>
<div class="modal fade" id="modal-modifcacion" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      {!! Form::open(
                      [ 'route' => [ 'inventariorepuestoadd' ],
                        'files' => true,
                        'enctype' => 'multipart/form-data',
                        "method" => "POST",
                        "class" => "form-horizontal",
                        "id" => "register-frm",
                        "accept-charset" => "UTF-8"
                      ])
      !!}
          <div class="modal-header">
            <h4 class="modal-title">Modificar Inventario Repuesto</h4>
          </div>
          <div class="modal-body" id="modal-body-container">
            	<div id="modal-body-loading"></div>
							<div id="modal-body-content">
	              <div class="form-group right">
	                  <label class="col-sm-4">Categoria Repuesto</label>
	                   <div class="col-sm-8 bold">{{ $inventario->categoria_nombre }}</div>
	              </div>
	              <div class="form-group right">
	                  <label class="col-sm-4">Nombre/Referencia Repuesto</label>
	                   <div class="col-sm-8 bold" id="referencia">{{ $inventario->repuesto_nombre }}</div>
	              </div>
	              <div class="form-group right">
	                  <label class="col-sm-4">Marca</label>
	                   <div class="col-sm-8 bold" id="marca">{{ $inventario->repuesto_marca }}</div>
	              </div>
	              <div class="form-group right">
	                  <label class="col-sm-4">Cantidad Registrada</label>
	                   <div class="col-sm-8 bold" id="cantidad">{{ $inventario->cantidad }}</div>
	              </div>
	              <div class="form-group right">
	                  <label class="col-sm-4">Cantidad A Adicionar</label>
	                   <div class="col-sm-8">
	                     <input type="text" value="" name="cantidad" class="form-control" maxlength="10" />
	                   </div>
	              </div>
	              <div class="form-group right">
	                  <label class="col-sm-4">Valor Actual</label>
	                   <div class="col-sm-8 bold" id="valor">{{ $inventario->valor }}</div>
	              </div>
	              <div class="form-group right">
	                  <label class="col-sm-4">Nuevo Valor</label>
	                   <div class="col-sm-8">
	                     <input type="text" value="" name="valor" class="form-control" maxlength="10" />
	                   </div>
	              </div>
							</div>
        </div>
        <div class="modal-footer">
          {!! Form::submit('Registrar',
                            [
                              'class' => 'btn btn-rw btn-primary',
                              'id' => 'modal-modifcacion-save'
                            ]
                          )
          !!}
          <button id="modal-modifcacion-cancel" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        </div>
      {!! Form::close() !!}
    </div>
  </div>
</div><?php
