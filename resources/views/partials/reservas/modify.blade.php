<script type="text/javascript">
	$(document).ready(function(){
		hideLoadingTr();

		$('input[type=button][value=Buscar]').on('click',function(){
			getRespuestosInventario();
		});
	});

	function getRespuestosInventario( ){
		$.ajax(
				{
						url: base_url + '/getrepuestosinventarioajax',
						data:{
										referencia: $('input[type=text][name=referencia]').val(),
										marca: $('input[type=text][name=marca]').val(),
										categoria: $('select[name=categoria_repuesto_id]').val(),
										_token: $('input[name=_token]').val()
									},
						cache: false,
						dataType:'json',
						//async:false,
						//processData: true,
						//contentType: 'json',
						type:'POST',
						beforeSend: function(){
							clearTable();
							$('input[type=button][value=Buscar]').attr('disabled',true);
							showLoadingTr();
						},
						success: function(response)
						{
							addToRepuestosTable(response);
							$('input[type=button][value=Buscar]').attr('disabled',false);
							hideLoadingTr();
							return true;
						},
						error: function(xhr, textStatus, errorThrown)
						{
								alert('error al intentar cargar los datos');
								return false;
						}
			});
		}

		function clearTable(){
			$('table.table tbody tr').each(function(index){
				if(index > 0){
					$(this).remove();
				}
			});
		}

		function hideLoadingTr(){
			$('tr[id=loading-tr]').remove();
		}

		function showLoadingTr(){
			var l = '<tr id="loading-tr"><td colspan="8">'+jsAjaxLoadingImageObjectString32+'</td></tr>';
			$('table.table tbody').append(l);
		}

		function addToRepuestosTable(jsonObject){

				var html = "";

				if( jsonObject.length > 0 ){

					$.each(jsonObject,function(index,element){

						html += '<tr data-element="'+element.id+'">';
						html += '<td>'+element.categoria_nombre+'</td>';
						html += '<td>'+element.repuesto_nombre+'</td>';
						html += '<td>'+element.repuesto_marca+'</td>';
						html += '<td>'+element.cantidad+'</td>';
						html += '<td>'+element.valor+'</td>';
						html += '<td>'+element.serial+'</td>';
						html += '<td>'+element.tipo+'</td>';
						html += '<td><a href="#" data-element="'+element.id+'" id="modificar-anchor-trigger'+element.id+'">Modificar</a></td>';
						html += '</tr>';
					});

					var head = '<tr><th>Categoría</th><th>Nombre ó Referencia</th>';
					head += '<th>Marca</th><th>Cantidad</th><th>Valor Unitario</th>';
					head += '<th>Número de Serie (Serial)</th>';
					head += '<th>Tipo</th><th>Acción</th></tr>';

					$('table.table thead tr').each(function(){
						$(this).remove();
					});

					$('table.table tbody tr').each(function(){
						$(this).remove();
					});

					$('table.table thead').append(head);
					$('table.table tbody').append(html);

					$('a[id^=modificar-anchor-trigger]').each(function(){
							$(this).bind('click',function(e){
								e.preventDefault();
								hideAllModificarAnchors();
								getRespuestosInventarioById($(this).attr('data-element'));
							});
					});

				}else{

					html += '<tr><td colspan="8">No se encontraron rerultados</td></td>';
					$('table.table').append(html);
				}



		}

		function hideAllModificarAnchors(){
			$('a[id^=modificar-anchor-trigger]').each(function(){
				$(this).hide();
			});
		}

		function showAllModificarAnchors(){
			$('a[id^=modificar-anchor-trigger]').each(function(){
				$(this).show();
			});
		}

		function getRespuestosInventarioById( id ){

			$.ajax({

				//url: base_url + '/getrepuestoinventariobyidajax',
				url: base_url + '/getrepuestoinventariobyidajax/'+id,
				data:{},
				cache: false,
				dataType:'html',
				//async:false,
				//processData: false,
				//contentType: 'html',
				//type:'POST',
				type:'GET',
				beforeSend: function(){

				},
				success: function(res, status, xhr)
				{
					$('div[id=modal-window-container]').html(res);
					return true;
				},
				error: function(xhr, textStatus, errorThrown)
				{
						alert('error al intentar cargar los datos');
						return false;
				}
			});
		}
</script>
