<script type="text/javascript">
	$(document).ready(function(){
		
		$("#register-frm").validate({
	        rules: {
	            razon_social: { required: true},
							//nit: { required: true},
							direccion: { required: true},
							telefono: { required: true},
							correo_electronico: {
																		required: true,
																		email: true
																	},
	        },
	        messages: {

	            razon_social: {
	                required: "Ingrese la Razón Social del prospecto"
	            },
							nit: {
	                required: "Ingrese el Nit del prospecto"
	            },
							direccion: {
	                required: "Ingrese la dirección del prospecto"
	            },
							telefono: {
	                required: "Ingrese el teléfono del prospecto"
	            },
							correo_electronico: {
	                required: "Ingrese la dirección de correo electrónico de la persona de contacto",
									email: "Debe ingresar una dirección de correo electrónico valida"
	            },
	        },

	        submitHandler: function(form) {
	            form.submit();
	        }
		});
	});
</script>
