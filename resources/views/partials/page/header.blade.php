<head>
    <meta charset="utf-8"/>
    <meta name="author" content="Deibi Octavio Riascos Botero" />
    <meta name="email" content="deibioctavio@gmail.com" />
    <meta name="description" content="Reservas Plus - Plataforma Para Reservas de Boletos en Salas de Cine y Teatros (Prueba Tecnica)">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="icon" href="{{ asset('assets/images/favicon_reservas.png') }}">

    <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <title>Reservas Plus - Plataforma Para Reservas de Boletos en Salas de Cine y Teatros</title>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <!-- Bootstrap Latest compiled and minified CSS -->
    {!! Html::style('assets/css/relaway/royal_preloader.css') !!}

    {!! Html::script('assets/js/relaway/jquery2.1.3.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/relaway/royal_preloader.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/jquery.validate.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/forms-customs-validations.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/jquery.confirm.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/jquery-ui.min.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/utils/customValidationRules.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/utils/commons.js', array('type' => 'text/javascript')) !!}
    {!! Html::script('assets/js/utils/download.js', array('type' => 'text/javascript')) !!}

    <script type="text/javascript">
        Royal_Preloader.config({
            mode:           'number',
            showProgress:   true,
            background:     '#ffffff'
        });

        var base_url = '<?php echo $app->make('url')->to('/')?>';

        var jsAjaxLoadingImageObjectString = '<img src="{{ asset('assets/images/loading.gif') }}" />';
        var jsAjaxLoadingImageObjectString16 = '<img width="16" height="16" src="{{ asset('assets/images/loading.gif') }}" />';
        var jsAjaxLoadingImageObjectString24 = '<img width="24" height="24" src="{{ asset('assets/images/loading.gif') }}" />';
        var jsAjaxLoadingImageObjectString32 = '<img width="32" height="32" src="{{ asset('assets/images/loading.gif') }}" />';
        var _imageMinWidth = {{ env('IMAGE_SIZE_WIDTH') }};
        var _imageMinHeight = {{ env('IMAGE_SIZE_HEIGHT') }};
        var _imageValidFormats = '{{ env('IMAGE_VALID_FORMATS') }}';
        var _SYSTEM_START_DATE = '{{ env('SYSTEM_START_DATE') }}';
        var _FECHA_HOY = '{{ date('Y-m-d') }}';

        $(document).ready(function(){

            $(".dropdown-menu > li > a.trigger").on("click", function(e) {

                $("ul[id=final-level]").each(function(){
                    $(this).slideUp();
                });

                var current = $(this).next();
                current.toggle();
                e.stopPropagation();
            });
        });

        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: 'yy-mm-dd',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
	 $.datepicker.setDefaults($.datepicker.regional['es']);

    </script>

    {!! Html::style('assets/css/relaway/bootstrap.min.css') !!}
    {!! Html::style('assets/css/relaway/style.css') !!}
    {!! Html::style('assets/css/relaway/bootstrap-social.css') !!}
    {!! Html::style('assets/css/relaway/animate.min.css') !!}
    {!! Html::style('assets/css/relaway/owl.carousel.css') !!}
    {!! Html::style('assets/css/relaway/jquery.snippet.css') !!}
    {!! Html::style('assets/css/relaway/buttons.css') !!}
    {!! Html::style('assets/css/relaway/colors/dark-blue.css') !!}
    {!! Html::style('assets/css/relaway/ionicons.min.css') !!}
    {!! Html::style('assets/css/relaway/font-awesome.css') !!}
    {!! Html::style('assets/css/relaway/magnific-popup.css') !!}
    @if (!isset($isReportView))
        {!! Html::style('assets/css/jumbotron-narrow.css') !!}
    @endif
    {!! Html::style('assets/css/jquery-ui.css') !!}
    {!! Html::style('assets/css/others.css') !!}
 </head>
