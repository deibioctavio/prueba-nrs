<li id="crud_admin" class="dropdown dropdown-main">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        Reservas
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu dropdown-subhover dropdown-animation animated fadeIn">
      <li class="dropdown dropdown-submenu">
          <a class="trigger">Reservas</a>
          <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
              <li>
                  <a tabindex="-1" href="{{route('reserva/add')}}">
                      Crear Reserva
                  </a>
              </li>
              <li>
                  <a tabindex="-1" href="{{route('reserva/all')}}">
                      Ver Todas Los Reservas
                  </a>
              </li>
          </ul>
      </li>
    </ul>
</li>
