<li id="crud_admin" class="dropdown dropdown-main">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        Administrar
        <span class="caret"></span>
    </a>
        <ul class="dropdown-menu dropdown-subhover dropdown-animation animated fadeIn">
            <!--
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Empresas</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                    <li>
                        <a tabindex="-1" href="{{route('empresaadd')}}">
                            Adicionar Empresa
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('empresashowall')}}">
                            Ver todas las Empresas
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Sucursales</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                    <li>
                        <a tabindex="-1" href="{{route('empresaadd')}}">
                            Adicionar Sucursal
                        </a>
                    </li>
                    <li>
                        <a tabindex="-1" href="{{route('empresashowall')}}">
                            Ver todas las Sucursales
                        </a>
                    </li>
                </ul>
            </li>
            -->
            <li class="dropdown dropdown-submenu">
                <a class="trigger">Perfiles</a>
                <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
                    <li>
                        <a tabindex="-1" href="{{route('userroleshowall')}}">
                            Ver Usuarios y Roles
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{route('auth/register')}}">Registrar usuarios</a>
            </li>
        </ul>
</li>
