<li id="crud_admin" class="dropdown dropdown-main">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        Administrar
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu dropdown-subhover dropdown-animation animated fadeIn">
    <li class="dropdown dropdown-submenu">
        <a class="trigger">Usuarios y Roles</a>
        <ul class="dropdown-menu animated fadeIn dropdown-animation" id="final-level">
            <li>
                <a tabindex="-1" href="{{route('userroleshowall')}}">
                    Ver Usuarios y Roles
                </a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{route('auth/register')}}">Registrar usuarios</a>
    </li>
    </ul>
</li>
