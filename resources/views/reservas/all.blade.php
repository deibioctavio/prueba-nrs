@extends('app')
<?php
  setlocale(LC_MONETARY, 'es_CO');
?>
@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Reservas</h2>
            <h4>Listado de Reservas Registradas</h4>
            <div class="form-group right">
                    <a href="{{route('reserva/add')}}" class="text-cyan">
                        Nueva Reserva
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th># de Reserva</th>
                            <th>Fecha de reserva</th>
                            <th>Pelicula</th>
                            <th>Teatro</th>
                            <th>Sala</th>
                            <th>Horario Función</th>
                            <th>Número de Personas</th>
                            <th>Butacas Reservadas</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($listadoReservas) < 1)
                            <tr>
                                <td colspan="9">No existen Reservas Registradas</td>
                            </tr>
                        @elseif (count($listadoReservas) >= 1)
                            @foreach ($listadoReservas as $a)
                                <tr>
                                    <td>{{ $a['numero'] }}</td>
                                    <td>{{ $a['fecha'] }}</td>
                                    <td>{{ $a['pelicula'] }}</td>
                                    <td>{{ $a['teatro'] }}</td>
                                    <td>{{ $a['sala'] }}</td>
                                    <td>{{ $a['horario'] }}</td>
                                    <td>{{ $a['numero_personas'] }}</td>
                                    <td>{{ $a['butacas'] }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
