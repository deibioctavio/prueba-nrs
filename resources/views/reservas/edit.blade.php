@extends('app')
@section('content')
@include('partials.maquina.edit')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Servicio (Orden de Trabajo)</h2>
            <h4>Diligencie la información del Servicio (Orden de Trabajo)</h4>

            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('maquinaedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" value="{{ $maquina->id }}" name="id">
                <div class="form-group right">
                    <label class="col-sm-4">Sucursal</label>
                     <div class="col-sm-8">
                        <select name="empresa_id">
                        <?php
                            foreach ($empresas as $r){
                              $selected = ($r->id == $maquina->empresa_id)?"selected":"";
                        ?>
                                <option value="{{ $r->id }}" {{ $selected }}>{{ $r->razon_social }}</option>
                        <?php
                            }
                        ?>
                        </select>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Categoría de Maquina</label>
                     <div class="col-sm-8">
                        <select name="categoria_maquina_id" id="categoria_maquina_id">
                          <?php
                              foreach ($categorias as $r){
                                $selected = ($r->id == $maquina->categoria_id)?"selected":"";
                          ?>
                                  <option value="{{ $r->id }}" {{ $selected }}>{{ $r->nombre }}</option>
                          <?php
                              }
                          ?>
                        </select>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Subcategoría de Maquina</label>
                     <div class="col-sm-8"><div id="subcategoria-container">
                        <select name="subcategoria_maquina_id">
                        <?php
                            foreach ($subcategorias as $r){
                              $selected = ($r->id == $maquina->subcategoria_maquina_id)?"selected":"";
                        ?>
                                <option value="{{ $r->id }}" {{ $selected }}>{{ $r->nombre }}</option>
                        <?php
                            }
                        ?>
                      </select></div>
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Nombre</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $maquina->nombre	 }}" name="nombre" class="form-control" maxlength="100">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Marca y/ó Modelo</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $maquina->marca }}" name="marca" class="form-control" maxlength="100">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Serial/Identificador (ID)</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $maquina->identificador }}" name="identificador" class="form-control" maxlength="50">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Valor inicial Consecutivo</label>
                    <div class="col-sm-8">
                        <input type="number" value="{{ $maquina->consecutivo }}" name="consecutivo" class="form-control" maxlength="10">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Estado: (<?php echo $maquina->active==1?"Activo":"Inactivo"?>) </label>
                    <?php
                        $selectedActivo =  ($maquina->active==1)?"selected":"";
                        $selectedInactivo =  ($maquina->active==0)?"selected":"";
                    ?>
                    <div class="col-sm-8">
                        <select name="active" >
                            <option value="1" <?php echo $selectedActivo?>>Activar</option>
                            <option value="0" <?php echo $selectedInactivo?>>Inactivar</option>
                        </select>
                    </div>
                </div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
