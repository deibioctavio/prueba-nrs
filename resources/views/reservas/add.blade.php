@extends('app')
@section('content')
@include('partials.reservas.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb10">
            <h2>Reserva de Boletos</h2>
            <h4>Diligencie la información de la  Reserva</h4>
            {!! Form::open(
              [
                  'id' => 'register-frm',
                  'route' => [ 'reserva/add' ],
                  'files' => true,
                  'enctype' => 'multipart/form-data',
                  "method" => "POST",
                  "class" => "form-horizontal",
                  "accept-charset" => "UTF-8"
              ]) !!}

                <div class="form-group right">
                    <label class="col-sm-4">Pelicula</label>
                    <select name="pelicula_id" id="pelicula_id">
                            @foreach ($peliculas as $p)
                                <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                            @endforeach
                        </select>
                </div>

                <div class="form-group right">
                    <label class="col-sm-4">Teatro</label>
                     <div class="col-sm-8">
                        <select name="teatro_id" id="teatro_id">
                            @foreach ($teatros as $t)
                                <option value="{{ $t->id }}">{{ $t->nombre }}</option>
                            @endforeach
                        </select>
                     </div>
                </div>
               
                <div class="form-group right">
                    <label class="col-sm-4">Sala</label>
                     <div class="col-sm-8">
                       <div id="salas-container"></div>
                       <input type="hidden" name="salas_id" value="" />
                     </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Horario (Función)</label>
                    <div class="col-sm-8">
                        <div id="horarios-container"></div>
                        <input type="hidden" name="horarios_id" value="" />
                    </div>
                </div>
                <hr class="mt10 mb10">
                <div class="form-group right">
                    <label class="col-sm-12">Reservas Previas para la función <div id="funcion-container"></div></label>                        
                    <div id="reservas-container"></div>
                </div>
                <hr class="mt10 mb10">
                <?php
                    for( $f = 1; $f <= getenv('NUMERO_MAXIMO_FILAS_SALA'); $f++ ){
                ?>
                <div class="form-group right" id="butacas-container-<?php echo $f?>">
                <?php
                        for( $c = 1; $c <= getenv('NUMERO_MAXIMO_COLUMNAS_SALA'); $c++ ){
                        $index = $f."_".$c;
                ?>
                        <div class="col-sm-1 butaca" id="bt_<?php echo $index; ?>">
                            <label for="bt_<?php echo $index; ?>"><?php echo "$f|$c"; ?></label>
                            <input id="bt_<?php echo $index; ?>" type="checkbox" value="<?php echo "$f|$c"; ?>" name="butacas[]" />
                        </div>
                <?php
                        }
                ?>
                </div>
                <hr class="mt4 mb4">
                <?php
                    }
                ?>
                </div>
                
               
            {!! Form::close() !!}
            <hr class="mt10 mb10">
            <div class="clear"></div>
            <div class="clear"></div>
            <hr class="mt10 mb10">
            <div>
                <input form="register-frm" type="submit" value="Reservar" class="btn btn-rw btn-primary center-block">
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
<div id="modal-window-container"></div>
@endsection
