@extends('app')

@section('content')
@include('partials.empresa.delete')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Prospecto Cliente</h2>
            <h4>Listado de Prospectos/Clientes Potenciales Registrados</h4>
            <div class="form-group right">
                    <a href="{{route('empresaadd')}}" class="text-cyan">
                        Adicionar Prospecto
                    </a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered report">
                    <thead>
                        <tr>
                            <th><center>#</center></th>
                            <th><center>Razón Social</center></th>
                            <th><center>Nit</center></th>
                            <th><center>Dirección</center></th>
                            <th><center>Teléfono</center></th>
                            <th><center>Nombre de la persona de contacto</center></th>
                            <th><center>Correo Electrónico de la persona de contacto</center></th>
                            <!--<th><center>Activa</center></th>-->
                            <th colspan="2"><center>Acción</center></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($empresas) < 1)
                            <tr>
                                <td colspan="9">No existen Empresas Registradas</td>
                            </tr>
                        @elseif (count($empresas) >= 1)
                            @foreach ($empresas as $i => $a)
                                <tr>
                                    <td>{{ ( ($empresas->currentPage() -1) * $empresas->perPage() )+$i+1 }}</td>
                                    <td>{{ $a->razon_social }}</td>
                                    <td>{{ $a->nit }}</td>
                                    <td>{{ $a->direccion }}</td>
                                    <td>{{ $a->telefono }}</td>
                                    <td>{{ $a->representante_legal }}</td>
                                    <td>{{ $a->correo_electronico }}</td>
                                    
                                    <!--<td><?php echo ($a->active==1)?"Si":"No"?></td>-->
                                    <td>
                                        <a href="<?php echo url('empresaedit',[$a->id])?>">Actualizar</a>
                                    </td>
                                    <td>
                                      <a id="empresa-del" number="<?php echo $a->id?>"  href="<?php echo url('empresadelete',[$a->id])?>">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                            <?php if( $empresas->total() > $empresas->perPage() ){ ?>
                              <tr><td colspan="9">{!! $empresas->render() !!}</td></tr>
                            <?php } ?>
                        @endif
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
