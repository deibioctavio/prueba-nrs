@extends('app')
@section('content')
@include('partials.empresa.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Prospecto Cliente</h2>
            <h4>Diligencie la información Cliente Potencial</h4>

            {!! Form::open([ 'route' => [ 'empresaadd' ], 'files' => true, 'enctype' => 'multipart/form-data',  "method" => "POST", "class" => "form-horizontal",
            "id" => "register-frm","accept-charset" => "UTF-8"]) !!}

                <div class="form-group right">
                    <label class="col-sm-4">Razon Social</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="razon_social" class="form-control" maxlength="100">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">NIT</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="nit" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Dirección</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="direccion" class="form-control" maxlength="50">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Teléfono</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="telefono" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Nombre de la persona de contacto</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="representante_legal" class="form-control" maxlength="100">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Correo Electrónico de la persona de contacto</label>
                    <div class="col-sm-8">
                        <input type="text" value="" name="correo_electronico" class="form-control" maxlength="100">
                    </div>
                </div>
                <div class="form-group right">&nbsp;</div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            {!! Form::close() !!}
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
