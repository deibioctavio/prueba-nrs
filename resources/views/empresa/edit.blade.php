@extends('app')
@section('content')
@include('partials.empresa.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                             <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <hr class="mt10 mb40">
            <h2>Prospecto Cliente</h2>
            <h4>Diligencie la información de la Empresa</h4>

            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('empresaedit', [])}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input type="hidden" value="{{ $empresa->id }}" name="id">
                <div class="form-group right">
                    <label class="col-sm-4">Razon Social</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $empresa->razon_social }}" name="razon_social" class="form-control" maxlength="100">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">NIT</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $empresa->nit }}" name="nit" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Dirección</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $empresa->direccion }}" name="direccion" class="form-control" maxlength="50">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Teléfono</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $empresa->telefono }}" name="telefono" class="form-control" maxlength="30">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Nombre de la persona de contacto</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $empresa->representante_legal }}" name="representante_legal" class="form-control" maxlength="100">
                    </div>
                </div>
                <div class="form-group right">
                    <label class="col-sm-4">Correo Electrónico de la persona de contacto</label>
                    <div class="col-sm-8">
                        <input type="text" value="{{ $empresa->correo_electronico }}" name="correo_electronico" class="form-control" maxlength="100">
                    </div>
                </div>
                
                <div class="form-group right">
                    <label class="col-sm-4">Estado: (<?php echo $empresa->active==1?"Activo":"Inactivo"?>) </label>
                    <?php
                        $selectedActivo =  ($empresa->active==1)?"selected":"";
                        $selectedInactivo =  ($empresa->active==0)?"selected":"";
                    ?>
                    <div class="col-sm-8">
                        <select name="active" >
                            <option value="1" <?php echo $selectedActivo?>>Activar</option>
                            <option value="0" <?php echo $selectedInactivo?>>Inactivar</option>
                        </select>
                    </div>
                </div>
                <div>
                    <input type="submit" value="Registrar" class="btn btn-rw btn-primary center-block">
                </div>
            </form>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
