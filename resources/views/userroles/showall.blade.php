@extends('app')

@section('content')
 <div class="container">
    <div class="row">
        <div class="col-sm-12">
            <hr class="mt10 mb40">
            <h2>Roles - Usuario</h2>
            <h4>Listado de Usuario y Sus Roles</h4>
            <div class="table-responsive">
                <table class="table table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre de usuario</th>
                            <th>Correo Electrónico</th>
                            <th>Rol</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        ?>
                            @foreach ($userRoles as $i =>$ur)
                                <tr>
                                    <td>{{ ( ($userRoles->currentPage() -1) * $userRoles->perPage() )+$i+1 }}</td>
                                    <td>{{ $ur->name }}</td>
                                    <td>{{ $ur->email }}</td>
                                    <td><?php echo isset($ur->role_name)?$ur->role_name:"Sin rol asignado";?>
                                    </td>
                                    <td>
                                        <a href="<?php echo url('userroleedit',[$ur->id])?>">Actualizar</a></td>
                                    </td>
                                </tr>
                            @endforeach
                            <?php if( $userRoles->total() > $userRoles->perPage() ){ ?>
                            	<tr><td colspan="5">{!! $userRoles->render() !!}</td></tr>
                            <?php } ?>
                    </tbody>
                </table>
            </div>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
