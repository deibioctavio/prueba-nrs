@extends('app')

@section('content')
@include('partials.datepicker')
@include('partials.profiles.add')
 <div class="container">
    <div class="row">
        <div class="col-sm-9">
            <hr class="mt10 mb40">
            <h2>Perfil</h2>
            <h4>Diligencie la información de la Sucursal a la que pertenece el usuario</h4>
            <form id="register-frm" class="form-horizontal" accept-charset="UTF-8" action="{{route('profileedit', [])}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div>
                  <label class="col-sm-3">Seleccione la Sucursal</label>
                   <div class="col-sm-9">
                      <select name="sucursal_id" id="sucursal_id"><?php
                        foreach ($sucursales as $s) {
                          $selected = ($s->id == $profile->empresa_id)?"selected":"";
                        ?>
                          <option value="<?php echo $s->id; ?>" <?php echo $selected;?>><?php echo $s->razon_social; ?></option>
                        <?php
                        }
                      ?></select>
                  </div>
                </div>
            </form>
            <hr class="mt10 mb10">
        </div>
    </div>
</div>
@endsection
