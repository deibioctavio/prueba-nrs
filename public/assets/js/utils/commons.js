var _width = 0;
var _height = 0;
var _msgStatus = null;

function imageCheckDimension(){

  if( _width >= _imageMinWidth && _height >= _imageMinHeight ){
    return {"status":"OK", 'width':_width,'height':_height};
  }else if ( _width < _imageMinWidth && _height < _imageMinHeight ){
    return {"status":"fail",'error_status':'both', 'width':_width,'height':_height};
  }else if( _width < _imageMinWidth ){
    return {"status":"fail",'error_status':'width', 'width':_width,'height':_height};
  }else{
    return {"status":"fail",'error_status':'height', 'width':_width,'height':_height};
  }
}

function fileGetExtension( fileObject ){

  var filename = $(fileObject).val().split('\\').pop();
  return filename.replace(/^.*\./, '');
}

function isImageExtension( stringValue ){

  var _validExtensions = _imageValidFormats.split('|');
  return jQuery.inArray( stringValue.toUpperCase(), _validExtensions )> -1;
}
