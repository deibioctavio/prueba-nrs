import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  'raleway-logo': {
    'paddingTop': [{ 'unit': 'px', 'value': 2 }],
    'height': [{ 'unit': 'px', 'value': 100 }],
    'width': [{ 'unit': 'px', 'value': 190 }]
  },
  'form-groupright': {
    'marginLeft': [{ 'unit': 'px', 'value': 0 }],
    'marginRight': [{ 'unit': 'px', 'value': 0 }]
  },
  'pgeneral-error': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 0 }],
    'fontWeight': 'bold'
  },
  'pgeneral-success': {
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 0 }],
    'fontWeight': 'bold'
  },
  'pinvisible': {
    'display': 'none'
  },
  'body': {
    'paddingBottom': [{ 'unit': 'px', 'value': 5 }],
    'paddingTop': [{ 'unit': 'px', 'value': 5 }]
  },
  'navbar-brand': {
    'height': [{ 'unit': 'px', 'value': 118 }],
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 15 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 15 }]
  },
  'navbar-nav': {
    'margin': [{ 'unit': 'px', 'value': 18 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': -18 }]
  },
  'navbar-toggle': {
    'marginTop': [{ 'unit': 'px', 'value': 40 }]
  },
  'error': {
    'color': '#ff0000'
  },
  'bold': {
    'fontWeight': 'bold'
  },
  'body': {
    'background': '#FFF'
  },
  // .navbar-nav, #navbar,.navbar {
	background: #FFF;
	color: #fff;
}
  'h1': {
    'color': '#34495E'
  },
  'h2': {
    'color': '#34495E'
  },
  'h3': {
    'color': '#34495E'
  },
  'h4': {
    'color': '#34495E'
  },
  'h5': {
    'color': '#34495E'
  },
  'h6': {
    'color': '#34495E'
  },
  'navbar ulnav a': {
    'color': '#34495E'
  },
  'row': {
    'color': '#34495E'
  },
  'select': {
    'color': '#555',
    'backgroundColor': '#fff'
  },
  'text-white': {
    'color': '#FFF',
    'fontWeight': 'bold'
  },
  'text-cyan': {
    'color': '#01CFFF',
    'fontWeight': 'bold'
  },
  'tablereport > tbody > tr > td': {
    'verticalAlign': 'middle',
    'textAlign': 'center'
  },
  'tablereport > tbody > tr > th': {
    'verticalAlign': 'middle',
    'textAlign': 'center'
  },
  'tablereport > tfoot > tr > td': {
    'verticalAlign': 'middle',
    'textAlign': 'center'
  },
  'tablereport > tfoot > tr > th': {
    'verticalAlign': 'middle',
    'textAlign': 'center'
  },
  'tablereport > thead > tr > td': {
    'verticalAlign': 'middle',
    'textAlign': 'center'
  },
  'tablereport > thead > tr > th': {
    'verticalAlign': 'middle',
    'textAlign': 'center'
  },
  'textarea': {
    'color': '#555'
  },
  'divtable-responsive td': {
    'verticalAlign': 'middle'
  },
  'div#images_count': {
    'fontWeight': 'bold'
  },
  'div#videos_count': {
    'fontWeight': 'bold'
  },
  'div#images_countgreen': {
    'color': '#2ecc71'
  },
  'div#videos_countgreen': {
    'color': '#2ecc71'
  },
  'div#images_countred': {
    'color': '#F00'
  },
  'div#videos_countred': {
    'color': '#F00'
  },
  'navbar-nav': {
    // background: url(../images/bg-h.jpg);
    'backgroundColor': '#34495E'
  },
  '#navbar': {
    // background: url(../images/bg-h.jpg);
    'backgroundColor': '#34495E'
  },
  'navbar': {
    // background: url(../images/bg-h.jpg);
    'backgroundColor': '#34495E'
  },
  'container-fluid': {
    // background: url(../images/bg-h.jpg);
    'backgroundColor': '#34495E'
  },
  'navnavbar-navnavbar-right': {
    'background': '#34495E',
    'border': [{ 'unit': 'px', 'value': 2 }],
    'borderColor': '#34495E',
    'borderStyle': 'solid'
  },
  'body': {
    'paddingTop': [{ 'unit': 'px', 'value': 0 }]
  },
  'navbar ulnav a': {
    'background': '#34495E none repeat scroll 0 0',
    'color': '#FFF',
    'fontWeight': 'bold',
    'margin': [{ 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }, { 'unit': 'px', 'value': 4 }],
    'borderRadius': '0px'
  },
  'navbar ulnav a:hover': {
    'color': '#494E51',
    'background': '#FFF none repeat scroll 0 0'
  },
  'navbar-toggle': {
    'background': '#f0d24e none repeat scroll 0 0!important',
    'borderColor': '#febb51 !important',
    'borderStyle': 'solid !important',
    'border': [{ 'unit': 'px', 'value': 2 }]
  },
  'navbar-toggle:hover': {
    'background': '#9b59b6 none repeat scroll 0 0!important'
  },
  'divrow-fluid a': {
    'float': 'left'
  },
  'clear': {
    'clear': 'both'
  },
  'loading-animation img': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  'row-fluidstrech': {
    'padding': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 0 }]
  },
  '::-webkit-scrollbar': {
    'WebkitAppearance': 'none'
  },
  'table#scrolling-table tbody': {
    'display': 'block',
    'height': [{ 'unit': 'px', 'value': 300 }],
    'overflow': 'auto'
  },
  'table#scrolling-table thead': {
    'display': 'table',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'tableLayout': 'fixed'
  },
  'table#scrolling-table tbody tr': {
    'display': 'table',
    'width': [{ 'unit': '%H', 'value': 1 }],
    'tableLayout': 'fixed'
  },
  'table#scrolling-table thead': {
    'width': [{ 'unit': '%H', 'value': NaN }]
  },
  'div#modal-modifcacion': {
    'textAlign': 'center'
  },
  'div#file-upload-link-list-container divrow-fluid': {
    'float': 'left',
    'height': [{ 'unit': 'px', 'value': 25 }],
    'margin': [{ 'unit': 'px', 'value': 0 }, { 'unit': 'px', 'value': 1 }, { 'unit': 'px', 'value': 2 }, { 'unit': 'px', 'value': 1 }],
    'width': [{ 'unit': 'px', 'value': 140 }]
  },
  'clear': {
    'clear': 'both'
  },
  'full-line': {
    'width': [{ 'unit': '%H', 'value': 1 }]
  },
  'div[id^=anchor-link]': {
    'textAlign': 'center'
  },
  'divimage-center img': {
    'marginLeft': [{ 'unit': 'string', 'value': 'auto' }],
    'marginRight': [{ 'unit': 'string', 'value': 'auto' }]
  },
  'divimage-center': {
    'minWidth': [{ 'unit': '%H', 'value': 0.9 }]
  },
  'divtext-center': {
    'textAlign': 'center'
  },
  'alert-success': {
    'marginTop': [{ 'unit': 'px', 'value': 10 }]
  },
  'alert-info': {
    'marginTop': [{ 'unit': 'px', 'value': 10 }]
  },
  'alert-warning': {
    'marginTop': [{ 'unit': 'px', 'value': 10 }]
  },
  'alert-danger': {
    'marginTop': [{ 'unit': 'px', 'value': 10 }]
  },
  'butaca': {
    'backgroundColor': '#00FF00'
  },
  'butaca-fill': {
    'backgroundColor': '#FF0000'
  },
  'butaca-fill-other': {
    'backgroundColor': '#0000FF'
  }
});
