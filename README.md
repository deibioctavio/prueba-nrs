## Requisitos para Instalación

- PHP 5.6 - 7.x
- Sistema de Gestión de Bases de Datos MySQL
- Composer (Instalado y accesible desde la línea de comandos)
- GIT (Para la descarga del Proyecto)
- Entorno de Desarrollo (LAMP, MAMP, WAMP)
- IDE (El de su preferencia)


## Detalles de Instalación

- Clone el repositorio completamente y establezca como Brach activo el master
- Una vez instalado mueva la carpeta al directorio web raiz (www, htdocs, public_html, el nombre varía de acuerdo al entorno empleado)
- Edite el archivo de configuración de variables de entorno .env ubicado en la raíz del directorio
- Dentro del archivo .env es importante que configure las variables DB_HOST, DB_DATABASE, DB_USERNAME, DB_PASSWORD, BASE_URL_PATH, BASE_PUBLIC_URL_PATH, BASE_DIR_PATH y BASE_PUBLIC_DIR_PATH
- Una vez configurado el archivo .env con las variables indicadas, proceda a crear la base de datos MySQL junto con el usuario de base de datos definido
- Abra una terminal de línea de comandos y ubiquese en el directorio raiz del proyecto
- Una vez ubicado en el raíz, ejecute los siguientes comando a fin de realizar el proceso de creación de tablas e ingreso de registro básicos. Ejecute: php artisan migrate y posteriormente ejecute php artisan db:seed
- Si no se presenta ningun inconveniente proceda a ingresar a la aplicación e ingrese a esta con las credeciales de acceso indicadas a continuación.


## Notas adicionales

- Cuando se realiza el proceso de migración de tablas y registro de datos básicos de la aplicación, se crea un usuario administrador por defecto. email: deibioctavio@gmail.com password: deibioctavio
- El proyecto requiere de la definición de unas variables adicionales, las cuales normalmente no son requieridas. Este cambio fue realizado ya que el framework laravel fue reconfigurado (hack) de tal manera que los proyectos realizados pudieran ser desplegados en servidores de tipo shared en los cuales normalmente no hay acceso al shell ni mucho menos a instalación de composer.
- Cualquier otra inquietud por favor dirija un correo a: deibioctavio@gamail.com