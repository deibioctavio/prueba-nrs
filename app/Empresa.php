<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'empresas';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   *
   *
   */
  protected $fillable = [
                          'razon_social',
                          'nit',
                          'direccion',
                          'telefono',
                          'correo_electronico',
                          'representante_legal',
                          'es_sucursal',
                          'sucursal_principal_id',
                          'active',
                        ];

  public function salas(){
        return $this->hasMany('App\Sala');
  }
}
