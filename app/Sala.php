<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sala extends Model
{
    protected $table = 'salas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
                            'nombre',
                            'observaciones',
                            'empresa_id',
                            'active',
                        ];

    public function empresa(){
        return $this->belongsTo('App\Empresa');
    }

    public function funciones(){
        return $this->hasMany('App\Funcion');
    }
}
