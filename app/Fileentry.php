<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fileentry extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   *
   *
   */
  protected $fillable = [
                          'filename',
                          'filesize',
                          'mime',
                          'original_filename',
                        ];

  public function inventario(){
        return $this->hasMany('App\InventarioRepuesto','imagen_id','id');
  }
}
