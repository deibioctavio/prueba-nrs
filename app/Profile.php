<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    protected $fillable = [
                            'name','lastname','document_type','document_number',
                            'birthday','address','gender','phone','cellphone'
                        ];

    public function user() {

       return $this->belongsTo('App\User');
    }

    public function ordenesTrabajoTecnicos(){
          return $this->hasMany('App\OrdenTrabajo','tecnico_id', 'id');
    }

    public function ordenesTrabajoCreo(){
          return $this->hasMany('App\OrdenTrabajo','usuario_id', 'id');
    }

    public function contactCenterEntries(){
      return $this->hasMany('App\ContactCenter','user_id', 'id');
      }

    
}
