<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reserva extends Model
{
    /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'reservas';

  protected $fillable = [
                          'numero_personas',
                          'user_id',
                          'funcion_id',
                          'active',
                      ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function funcion(){
        return $this->belongsTo('App\Funcion');
    }
}
