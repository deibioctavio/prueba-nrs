<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcion extends Model
{
    protected $table = 'funciones';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
                            'nombre',
                            'fecha_inicio',
                            'fecha_fin',
                            'pelicula_id',
                            'sala_id',
                            'active',
                        ];

    public function sala(){
        return $this->belongsTo('App\Sala');
    }

    public function pelicula(){
        return $this->belongsTo('App\Pelicula');
    }

    public function reservas(){
        return $this->hasMany('App\Reserva');
    }
}
