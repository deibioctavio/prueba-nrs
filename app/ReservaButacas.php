<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaButacas extends Model
{
    /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'reserva_butacas';

  protected $fillable = [
                          'fila_butaca',
                          'columna_butaca',
                          'reserva_id',
                          'active',
                      ];
}
