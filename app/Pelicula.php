<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelicula extends Model
{
     /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'peliculas';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   *
   *
   */
  protected $fillable = [
                          'nombre',
                          'sinopsis',
                          'director',
                          'idioma',
                          'clasificacion',
                          'duracion_minutos',
                          'ano',
                          'fecha_salida',
                          'active',
                        ];

  public function funciones(){
      return $this->hasMany('App\Funcion');
  }
}
