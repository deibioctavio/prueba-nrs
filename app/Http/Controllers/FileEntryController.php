<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Fileentry;
use Illuminate\Support\Facades\Storage;
use File;
use App\OrdenTrabajo;
use Illuminate\Http\Response;
use Intervention\Image\Image as Image;

class FileEntryController extends Controller
{
    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$files = Fileentry::all();

		return view('fileentries.index', compact('files'));
	}


	public function add() {

		$file = Request::file('filefield');
		$extension = $file->getClientOriginalExtension();
		Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
		$entry = new Fileentry();
		$entry->mime = $file->getClientMimeType();
		$entry->original_filename = $file->getClientOriginalName();
		$entry->filename = $file->getFilename().'.'.$extension;
		$entry->filesize = $file->getSize();
    $entry->extension = $extension;
		$entry->save();
		//echo json_encode(array( "filename" =>$entry->filename));
		return redirect('fileentry');
	}

	public function addAjax() {

		$elementId = Request::input('element_id');
		$file = Request::file('filefield'.$elementId);
		$extension = $file->getClientOriginalExtension();
		Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
		$entry = new Fileentry();
		$entry->mime = $file->getClientMimeType();
		$entry->original_filename = $file->getClientOriginalName();
		$entry->filename = $file->getFilename().'.'.$extension;
		$entry->filesize = $file->getSize();
    $entry->extension = $extension;
		$entry->save();
		echo json_encode(array("filedata" =>$entry,"element_id"=>$elementId));
	}

  public function getAjax($id){

    $entry = Fileentry::where('id', '=', $id)->firstOrFail();
		$file = Storage::disk('local')->get($entry->filename);
    return response(base64_encode($file), 200)
           ->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0')
           ->header('Content-Description', 'File Transfer')
           ->header('Content-Disposition', 'attachment; filename='.$entry->original_filename)
           ->header('Content-Type', $entry->mime);
  }

  public function get($id){

    $entry = Fileentry::where('id', '=', $id)->firstOrFail();
    $path = storage_path('app').'/'.$entry->filename;
    if (file_exists($path)) {
        return response()->download($path);
    }
  }

  public function getImage($id){

    $entry = Fileentry::where('id', '=', $id)->firstOrFail();

    $tmpFileName = 'file'.date('Ymd').'.tmp';
    $oldPath = storage_path('app').'/' .$entry->filename;
    $newPath = public_path().'/tmp/'.$tmpFileName;

    $headers = ['Content-Type' => $entry->mime];

    if (File::copy($oldPath , $newPath)) {
        $r = response()->download($newPath, $entry->original_filename, $headers);
        return $r->deleteFileAfterSend(true);
    }
  }

  public function getPdf($id){

    $entry = Fileentry::where('id', '=', $id)->firstOrFail();
    $tmpFileName = 'file'.date('Ymd').'.tmp';
    $oldPath = storage_path('app').'/' .$entry->filename;
    $newPath = public_path().'/tmp/'.$tmpFileName;

    $ordenTrabajo = OrdenTrabajo::find($id);
    $newName = "ordentrabajo-".$ordenTrabajo->created_at.".pdf";

    $headers = ['Content-Type' => $entry->mime];

    if (File::copy($oldPath , $newPath)) {
        $r = response()->download($newPath, $newName, $headers);
        return $r->deleteFileAfterSend(true);
    }
  }

  public function getPdfByFilename($filename){

    $oldPath = storage_path('app').'/' .$filename;
    $newPath = public_path().'/tmp/'.$tmpFileName;

    $headers = ['Content-Type' => $entry->mime];

    if (File::copy($oldPath , $newPath)) {
        $r = response()->download($newPath, $newName, $headers);
        return $r->deleteFileAfterSend(true);
    }
  }

  public function deleteAjax($id){

    $header = array (
              'Content-Type' => 'application/json; charset=UTF-8',
              'charset' => 'utf-8'
          );

    $entry = Fileentry::where('id', '=', $id)->firstOrFail();

		if( $fD = Storage::disk('local')->Delete($entry->filename) ){
      $fE = Fileentry::find($id);
      $rD = $fE->delete();
      return response()->json(['file-delete'=>$fD,'row-delete'=>$rD], 200, $header, JSON_UNESCAPED_UNICODE);
    }else{
      return response()->json($vd, 500, $header, JSON_UNESCAPED_UNICODE);
    }
  }
}
