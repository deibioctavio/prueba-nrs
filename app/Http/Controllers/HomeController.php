<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $collection = collect(array(39,40))->implode('|');

        return \View::make('home',['implode'=>$collection]);
    }
}
