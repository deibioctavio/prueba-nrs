<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Profile;
use Bican\Roles\Models\Role;
use DB;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*$roles = Role::get();
        $users = User::get();
        $user = User::where('id',Auth::user()->id)->first();
        $userRoles = array();*/

        $userRoles =  DB::table('users AS U')
                      ->leftJoin('role_user AS UR','U.id','=','UR.user_id')
                      ->leftJoin('roles AS R','UR.role_id','=','R.id')
                      ->orderBy('U.name')
                      ->orderBy('U.email')
                      ->select('U.*','R.name AS role_name')
                      ->paginate(env('PAGINATION_PAGE_ROWS'));

        return \View::make('userroles.showall',['userRoles'=>$userRoles,]);
        //print_r(Auth::user()->toArray());
        //print_r($user->hasRole('moderador'));
        //var_dump($userRoles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::get();
        $user = User::where('id',$id)->first();

        $usuarioRol['name'] = "";
        $usuarioRol['id'] = 0;


        foreach ($roles as $r) {

            if($user->hasRole($r->slug)){
                $usuarioRol['id'] = $r->id;
                $usuarioRol['name'] = $r->name;
            }
        }

        return \View::make('userroles.edit',array('roles' => $roles,'user'=>$user,'usuarioRol'=>$usuarioRol));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id = $request->input('user_id');
        $rol_id = $request->input('rol_id');

        $user = User::where('id', $user_id)->first();

        if( $rol_id != 0){

            $role = Role::where('id', $rol_id)->first();

            if($role && $user){
                $user->detachAllRoles();
                $user->attachRole($role);
            }
        }else{

            if($user){
                $user->detachAllRoles();
            }
        }


        return redirect('userroleshowall');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
