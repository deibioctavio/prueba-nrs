<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Empresa;
use DB;

class SucursalController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $empresas = DB::table('empresas as S')
            ->join('empresas as E', 'S.sucursal_principal_id', '=', 'E.id')
            //->where('S.active', 1)
            ->where('S.es_sucursal', 1)
            ->whereNotNull('S.sucursal_principal_id')
            ->select('S.*', 'E.razon_social as empresa_matriz')
            ->paginate(env('PAGINATION_PAGE_ROWS'));

    return \View::make('sucursal.showall',
                                           [
                                              'empresas' => $empresas,
                                              'isReportView'=>true
                                            ]
                     );
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      $empresas = Empresa::where('es_sucursal',0)->where('active',1)->get();
      return \View::make('sucursal.add',['empresas' => $empresas ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $empresa = new Empresa();
    $empresa->razon_social = $request->input('razon_social');
    $empresa->nit = $request->input('nit');
    $empresa->direccion = $request->input('direccion');
    $empresa->telefono = $request->input('telefono');
    $empresa->correo_electronico = $request->input('correo_electronico');
    $empresa->representante_legal = $request->input('representante_legal');
    $empresa->es_sucursal = 1;
    $empresa->sucursal_principal_id = $request->input('sucursal_principal_id');

    if(!$empresa->save()){

        return redirect()->back()->withError('Error guardando la información de la sucursal en la base de datos');

    }else{

        $request->session()->flash('flash_success_message', env('FLASH_SUCCESS_MESSAGE'));
        return redirect('sucursalshowall');
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {

  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $empresas = Empresa::where('es_sucursal',0)->get();
    $sucursal = Empresa::where('id',$id)->first();
    return \View::make('sucursal.edit',['empresas' => $empresas,'sucursal' => $sucursal]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request)
  {
    $id = $request->input('id');
    $empresa = Empresa::find($id);
    $empresa->razon_social = $request->input('razon_social');
    $empresa->nit = $request->input('nit');
    $empresa->direccion = $request->input('direccion');
    $empresa->telefono = $request->input('telefono');
    $empresa->correo_electronico = $request->input('correo_electronico');
    $empresa->representante_legal = $request->input('representante_legal');
    $empresa->es_sucursal = 1;
    $empresa->active = $request->input('active');
    $empresa->sucursal_principal_id = $request->input('sucursal_principal_id');

    if(!$empresa->save()){

        return redirect()->back()->withError('Error guardando la información de la sucursal en la base de datos');

    }else{

        $request->session()->flash('flash_success_message', env('FLASH_SUCCESS_MESSAGE'));
        return redirect('sucursalshowall');
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
       $empresa = Empresa::find($id);
       $empresa->delete();
       return redirect('sucursalshowall');
  }
}
