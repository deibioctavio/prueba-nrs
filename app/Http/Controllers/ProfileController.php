<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Config\Repository;
use Bican\Roles\Models\Permission;
use Auth;
use App\User;
use App\Profile;
use App\Empresa;
use DB;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $profile = Profile::where('user_id',Auth::user()->id)->first();

        if( count($profile) < 1){
            return redirect('profileadd');
        }else{
            return redirect('profileedit');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return \View::make('profiles.add',['user' => Auth::user(),/*'sucursales' => $sucursales,*/]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $profile = new Profile();

        $profile->name = $request->input('name');
        $profile->lastname = $request->input('lastname');
        $profile->document_type = $request->input('document_type');
        $profile->document_number = $request->input('document_number');
        $profile->birthday = $request->input('birthday');
        $profile->address = $request->input('address');
        $profile->gender = $request->input('gender');
        $profile->phone = $request->input('phone');
        $profile->cellphone = $request->input('cellphone');
        //$profile->empresa_id = $request->input('empresa_id');
        $profile->user_id = Auth::user()->id;

        if(!$profile->save()){

            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect()->back();

        }else{

            $request->session()->flash('flash_success_message', env('FLASH_SUCCESS_MESSAGE'));
            return redirect('profileedit');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id = null)
    {
        $profile_id = ($id == null)?Auth::user()->id:$id;

        $profile = Profile::where('user_id',$profile_id)->first();
        //$sucursales = Empresa::where('es_sucursal',1)->orderBy('razon_social','ASC')->get();

        return \View::make('profiles.edit',
                                array(
                                        'user' => Auth::user(),
                                        'profile'=>$profile,
                                        //'sucursales' => $sucursales,
                                    )
                            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {
        $profile = Profile::where('user_id',Auth::user()->id)->first();

        $profile->name = $request->input('name');
        $profile->lastname = $request->input('lastname');
        $profile->document_type = $request->input('document_type');
        $profile->document_number = $request->input('document_number');
        $profile->birthday = $request->input('birthday');
        $profile->address = $request->input('address');
        $profile->gender = $request->input('gender');
        $profile->phone = $request->input('phone');
        $profile->cellphone = $request->input('cellphone');
        //$profile->empresa_id = $request->input('empresa_id');

        if(!$profile->save()){

            $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
            return redirect('profileedit');

        }else{

            $request->session()->flash('flash_success_message', env('FLASH_SUCCESS_MESSAGE'));
            return redirect('profileedit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function getFullProfileDataByUserId( Request $request){
        $id = $request->input('id');

        $profile = DB::table('profiles')
            ->where('profiles.document_number','like',"%{$id}%")
            ->select('profiles.*')
            ->get();

        $data = array('profile'=>$profile);
        return response()->json($data);
    }

    function setSucursal($id){

      $profile = Profile::where('user_id',$id)->first();
      $sucursales = Empresa::where('es_sucursal',1)->get();

      return \View::make('profiles.setsucursal',
                              array(
                                      'profile'=>$profile,
                                      'sucursales' => $sucursales
                                  )
                          );
    }

    function saveSucursal(Request $request){
      $profile = Profile::find($id);

      $profile->empresa_id = $request->input('sucursal_id');

      if(!$profile->save()){

          $request->session()->flash('flash_error_message', 'Ocurrió un error insertando el registro');
          return redirect('profileeditaddsucursal/'.$id);

      }else{

          $request->session()->flash('flash_success_message', env('FLASH_SUCCESS_MESSAGE'));
          return redirect('profileeditaddsucursal/'.$id);
      }
    }
}
