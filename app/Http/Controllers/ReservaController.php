<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Log;
use App\Empresa;
use App\Pelicula;
use App\Sala;
use App\Funcion;
use App\Reserva;
use App\ReservaButacas;
use Auth;
use Html;
use App\Fileentry;
use Illuminate\Support\Facades\Storage;
use File;

class ReservaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $reservas = Reserva::where('user_id',Auth::user()->id)->orderBy('id','ASC')->get();

      $i = 0;
      $listadoReservas = array();
      foreach( $reservas as $reserva ){

        $funcion = Funcion::where('id',$reserva->funcion_id)->first();
        $pelicula = Pelicula::where('id',$funcion->pelicula_id)->first();
        $sala = Sala::where('id',$funcion->sala_id)->first();
        $teatro = Empresa::where('id',$sala->empresa_id)->first();

        $rButacas = ReservaButacas::
                            where('user_id',Auth::user()->id)
                            ->where('reserva_id',$reserva->id)
                            ->orderBy('id','ASC')
                            ->get();

        //dd($rButacas);

        $butacas = array();

        $j = 0;
        foreach( $rButacas as $b ){
          
          $butacas[$j] = "[$b->fila_butaca|$b->columna_butaca]";
          $j++;
        }

        $listadoReservas[$i]['numero'] = $reserva->id;
        $listadoReservas[$i]['fecha'] = $reserva->created_at;
        $listadoReservas[$i]['numero_personas'] = $reserva->numero_personas;
        $listadoReservas[$i]['pelicula'] = $pelicula->nombre;
        $listadoReservas[$i]['teatro'] = $teatro->nombre;
        $listadoReservas[$i]['sala'] = $sala->observaciones;
        $listadoReservas[$i]['horario'] = $funcion->fecha_inicio;
        $listadoReservas[$i]['butacas'] = implode(", ",$butacas);
      
        $i++;
      }

      //dd($listadoReservas);

      return \View::make('reservas.all',
                            [
                              'listadoReservas' => $listadoReservas,
                              'isReportView'=>true
                            ]
                        );
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $peliculas = Pelicula::orderBy('nombre','ASC')->get();
      $teatros = Empresa::orderBy('nombre','ASC')->get();
        
      return \View::make('reservas.add',
                            [
                              'teatros' => $teatros,
                              'peliculas' => $peliculas,
                              'isReportView'=>true
                            ]
                        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
              DB::beginTransaction();
              $reserva = new Reserva();
              $reserva -> numero_personas = count($request->input('butacas'));
              $reserva -> user_id = Auth::user()->id;
              $reserva -> funcion_id = $request->input('funcion_id');

              if ( $reserva->save() ){ //IF GUARDADO ORDEN DE TRABAJO EXITOSO

                $fileSting = "";
                $fileSting .= "|reserva_id:".$reserva -> id;
                $fileSting .= "|numero_personas:".$reserva -> numero_personas;
                $fileSting .= "|user_id:".$reserva -> user_id;
                $fileSting .= "|funcion_id:".$reserva -> funcion_id;
                $fileSting .= "|user_id:".$reserva -> user_id;

                $butacas = $request->input('butacas');

                $stB = print_r($butacas,TRUE);

                //Log::info('bArray: '.$stB);

                for ($i = 0; $i < $reserva -> numero_personas; $i++){

                  $b = $butacas[$i];

                  $butacasReserva = new ReservaButacas();

                  $bArray = explode('|',$b);
                  $butacasReserva -> fila_butaca = $bArray[0];
                  $butacasReserva -> columna_butaca = $bArray[1];
                  $butacasReserva -> reserva_id = $reserva->id;
                  $butacasReserva -> user_id = Auth::user()->id;

                  //Log::info('bString: '.$b);

                  if ( !$butacasReserva->save() ){

                    DB::rollBack();
                    Log::error('Error registrando la información de las butacas de la reserva en la base de datos.');
                    return redirect()->back()->withError('Error registrando la información de las butacas de la reserva en la base de datos.');
                  }  
                  
                  $fileSting .= "|butaca_id:".$butacasReserva -> id;
                  $fileSting .= "|fila_butaca:".$butacasReserva -> fila_butaca;
                  $fileSting .= "|columna_butaca:".$butacasReserva -> columna_butaca;
                  $fileSting .= "|reserva_id:".$butacasReserva -> reserva_id;                    
                }

                Storage::disk('local')->put('reserva_'.$reserva -> id.".txt",$fileSting);
                $request->session()->flash('flash_success_message', env('FLASH_SUCCESS_MESSAGE'));
                DB::commit();
                return redirect('reserva/all/');
              }
              else{

                DB::rollBack();
                Log::info('Error registrando la información de la reserva en la base de datos.');
                return redirect()->back()->withError('Error registrando la información de la reserva en la base de datos.');
              }

        }catch(\Illuminate\Database\QueryException $ex){

          DB::rollBack();
          Log::info($ex->getMessage());
          return redirect()->back()->withError('Error registrando la información de la reserva en la base de datos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $ordenTrabajo = DB::table('orden_trabajos AS OT')
                        ->join('profiles AS P','OT.tecnico_id','=','P.user_id')
                        ->join('maquinas AS M','OT.maquina_id','=','M.id')
                        ->join('subcategoria_maquinas AS SCM',
                                'M.subcategoria_maquina_id','=','SCM.id'
                              )
                        ->join('categoria_maquinas AS CM',
                                'SCM.categoria_maquina_id','=','CM.id'
                              )
                        ->where('OT.id', $id)
                        ->select(
                                  'OT.*',
                                  'M.nombre AS maquina_nombre',
                                  'M.identificador AS maquina_identificador',
                                  'M.marca AS maquina_marca',
                                  'SCM.nombre AS subcategoria_maquina_nombre',
                                  'CM.nombre AS categoria_maquina_nombre',
                                  'P.name AS tecnico_nombre',
                                  'P.lastname AS tecnico_apellido'
                                )
                        ->first();

      $repuestos = DB::table('orden_trabajo_repuestos AS OTR')
                        ->join('repuestos AS R','OTR.repuesto_id','=','R.id')
                        ->join('categoria_repuestos AS CR',
                                'R.categoria_repuesto_id','=','CR.id'
                              )
                        ->where('OTR.orden_trabajo_id', $id)
                        ->orderBy('OTR.id','ASC')
                        ->select(
                                  'OTR.*',
                                  'R.referencia AS referencia',
                                  'R.marca AS marca',
                                  'CR.nombre AS categoria'
                                )
                        ->get();

      $images = array_filter(
                              explode('|',$ordenTrabajo->imagen_id),
                                function( $element ){
                                    return (strlen($element) > 0);
                              });

      return \View::make('ordentrabajo.show',
                            [
                              'ordenTrabajo' => $ordenTrabajo,
                              'repuestos' => $repuestos,
                              'images' => $images,
                              'isReportView'=>true
                            ]
                        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generatePdf($id)
    {
      $ordenTrabajo = DB::table('orden_trabajos AS OT')
                        ->join('profiles AS P','OT.tecnico_id','=','P.user_id')
                        ->join('maquinas AS M','OT.maquina_id','=','M.id')
                        ->join('subcategoria_maquinas AS SCM',
                                'M.subcategoria_maquina_id','=','SCM.id'
                              )
                        ->join('categoria_maquinas AS CM',
                                'SCM.categoria_maquina_id','=','CM.id'
                              )
                        ->where('OT.id', $id)
                        ->select(
                                  'OT.*',
                                  'M.nombre AS maquina_nombre',
                                  'M.identificador AS maquina_identificador',
                                  'M.marca AS maquina_marca',
                                  'SCM.nombre AS subcategoria_maquina_nombre',
                                  'CM.nombre AS categoria_maquina_nombre',
                                  'P.name AS tecnico_nombre',
                                  'P.lastname AS tecnico_apellido'
                                )
                        ->first();

      $repuestos = DB::table('orden_trabajo_repuestos AS OTR')
                        ->join('repuestos AS R','OTR.repuesto_id','=','R.id')
                        ->join('categoria_repuestos AS CR',
                                'R.categoria_repuesto_id','=','CR.id'
                              )
                        ->where('OTR.orden_trabajo_id', $id)
                        ->orderBy('OTR.id','ASC')
                        ->select(
                                  'OTR.*',
                                  'R.referencia AS referencia',
                                  'R.marca AS marca',
                                  'CR.nombre AS categoria'
                                )
                        ->get();

      $images = array_filter(
                              explode('|',$ordenTrabajo->imagen_id),
                                function( $element ){
                                    return (strlen($element) > 0);
                              });

      $view = \View::make('ordentrabajo.pdf',
                            [
                              'ordenTrabajo' => $ordenTrabajo,
                              'repuestos' => $repuestos,
                              'images' => $images,
                              'isReportView'=>true
                            ]
                        )->render();

      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('orden-trabajo-'.date('Y-m-d').'.pdf');
    }

    private function autosavePdf( $id ){
      $ordenTrabajo = DB::table('orden_trabajos AS OT')
                        ->join('profiles AS P','OT.tecnico_id','=','P.user_id')
                        ->join('maquinas AS M','OT.maquina_id','=','M.id')
                        ->join('subcategoria_maquinas AS SCM',
                                'M.subcategoria_maquina_id','=','SCM.id'
                              )
                        ->join('categoria_maquinas AS CM',
                                'SCM.categoria_maquina_id','=','CM.id'
                              )
                        ->where('OT.id', $id)
                        ->select(
                                  'OT.*',
                                  'M.nombre AS maquina_nombre',
                                  'M.identificador AS maquina_identificador',
                                  'M.marca AS maquina_marca',
                                  'SCM.nombre AS subcategoria_maquina_nombre',
                                  'CM.nombre AS categoria_maquina_nombre',
                                  'P.name AS tecnico_nombre',
                                  'P.lastname AS tecnico_apellido'
                                )
                        ->first();

      $repuestos = DB::table('orden_trabajo_repuestos AS OTR')
                        ->join('repuestos AS R','OTR.repuesto_id','=','R.id')
                        ->join('categoria_repuestos AS CR',
                                'R.categoria_repuesto_id','=','CR.id'
                              )
                        ->where('OTR.orden_trabajo_id', $id)
                        ->orderBy('OTR.id','ASC')
                        ->select(
                                  'OTR.*',
                                  'R.referencia AS referencia',
                                  'R.marca AS marca',
                                  'CR.nombre AS categoria'
                                )
                        ->get();

      $images = array_filter(
                              explode('|',$ordenTrabajo->imagen_id),
                                function( $element ){
                                    return (strlen($element) > 0);
                              });

      $view = \View::make('ordentrabajo.pdf',
                            [
                              'ordenTrabajo' => $ordenTrabajo,
                              'repuestos' => $repuestos,
                              'images' => $images,
                              'isReportView'=>true
                            ]
                        )->render();

      $pdf = \App::make('dompdf.wrapper');

      $pdfName = md5(env('DEFAULT_OT_FILE_FREFIX').$id).'.pdf';

      $pdf->loadHTML($view)->save(storage_path('app').'/'.$pdfName);
      $file = Storage::disk('local')->get($pdfName);
      $entry = new Fileentry();
  		$entry->mime = 'application/pdf';
  		$entry->original_filename = $pdfName;
  		$entry->filename = $pdfName;
  		$entry->filesize = Storage::size($pdfName);
      $entry->extension = 'pdf';
  		$entry->save();
    }

    public function getOrdenTrabajoNombreByOtId($id){

      return DB::table('orden_trabajos AS OT')
                    ->join('maquinas AS M','OT.maquina_id','=','M.id')
                    ->join(
                            'subcategoria_maquinas AS SCM','M.subcategoria_maquina_id',
                            '=',
                            'SCM.id')
                    ->join(
                            'categoria_maquinas AS CM',
                            'SCM.categoria_maquina_id',
                            '=',
                            'CM.id')
                    ->where('OT.id',$id)
                    ->select(
                              'OT.*',
                              'OT.consecutivo AS consecutivo',
                              'M.nombre AS maquina_nombre',
                              'SCM.nombre AS subcategoria_nombre',
                              'CM.nombre AS categoria_nombre'
                            )
                    ->first();
    }

    public function getPdfById($id){

      $datosNombre = $this->getOrdenTrabajoNombreByOtId($id);

      $tmpFileName = santize_filename($datosNombre->categoria_nombre);
      $tmpFileName .= santize_filename('-'.$datosNombre->subcategoria_nombre);
      $tmpFileName .= santize_filename('-'.$datosNombre->maquina_nombre);
      $tmpFileName .= santize_filename('-'.$datosNombre->consecutivo).'.pdf';

      $pdfName = md5(env('DEFAULT_OT_FILE_FREFIX').$id).'.pdf';
      $entry = Fileentry::where('filename', '=', $pdfName)->firstOrFail();
      $oldPath = storage_path('app').'/' .$pdfName;
      $newPath2 = public_path().'/tmp/'.$tmpFileName;
      $newPath = public_path().'/tmp/'.date('YmdHis').'.pdf';

      $headers = ['Content-Type' => $entry->mime];

      if (File::copy($oldPath , $newPath)) {
          $r = response()->download($newPath, $tmpFileName, $headers);
          return $r->deleteFileAfterSend(true);
      }
    }


    public function getByFuncion(){
      $reservas = Reserva::where('user_id',Auth::user()->id)->orderBy('id','ASC')->get();
    }

    public function getButacasByFuncion( $funcionId ){    
      
      $header = [
        'Content-Type' => 'application/json',
        'charset' => 'utf-8'
      ];

      $rButacas = array();
      $reservas = Reserva::
                    where('funcion_id',$funcionId)
                    ->where('user_id',Auth::user()->id)
                    ->get();

      if (!$reservas ){
        //Log::error('ERROR RESERVA NULL');
      }else{
       // Log::info('reservas: '.print_r($reservas,TRUE));

        foreach( $reservas as $res ){

          $rb = ReservaButacas::
                            where('user_id',Auth::user()->id)
                            ->where('reserva_id',$res->id)
                            ->orderBy('id','ASC')
                            ->get();

          foreach( $rb as $r ){

            $butaca = array();
            $butaca['fila_butaca'] = $r->fila_butaca;
            $butaca['columna_butaca'] = $r->columna_butaca;
            $butaca['reserva_id'] = $r->reserva_id;
            $butaca['user_id'] = $r->user_id;
      
            array_push($rButacas,$butaca);
          }
        }

        //Log::info('RB: '.print_r($rButacas,TRUE));
        
      }

      return response()->json($rButacas, 200, $header, JSON_UNESCAPED_UNICODE);  
    }

    public function getOtrasButacasByFuncion( $funcionId ){    
      
      $header = [
        'Content-Type' => 'application/json',
        'charset' => 'utf-8'
      ];

      $rButacas = array();
      $reservas = Reserva::
                    where('funcion_id',$funcionId)
                    ->where('user_id','<>',Auth::user()->id)
                    ->get();

      if (!$reservas ){
        //Log::error('ERROR RESERVA NULL');
      }else{
       // Log::info('reservas: '.print_r($reservas,TRUE));

        foreach( $reservas as $res ){

          $rb = ReservaButacas::
                            where('reserva_id',$res->id)
                            ->orderBy('id','ASC')
                            ->get();

          foreach( $rb as $r ){

            $butaca = array();
            $butaca['fila_butaca'] = $r->fila_butaca;
            $butaca['columna_butaca'] = $r->columna_butaca;
            $butaca['reserva_id'] = $r->reserva_id;
            $butaca['user_id'] = $r->user_id;
      
            array_push($rButacas,$butaca);
          }
        }

        //Log::info('RB: '.print_r($rButacas,TRUE));
        
      }

      return response()->json($rButacas, 200, $header, JSON_UNESCAPED_UNICODE);  
    }

    
}
