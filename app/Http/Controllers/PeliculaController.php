<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Log;
use App\Empresa;
use App\Pelicula;
use App\Sala;
use App\Funcion;
use Auth;
use Html;
use App\Fileentry;
use Illuminate\Support\Facades\Storage;
use File;

class PeliculaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $ordenesTrabajo = DB::table('orden_trabajos AS OT')
                        ->join('profiles AS P','OT.tecnico_id','=','P.user_id')
                        ->join('maquinas AS M','OT.maquina_id','=','M.id')
                        ->join('subcategoria_maquinas AS SCM',
                                'M.subcategoria_maquina_id','=','SCM.id'
                              )
                        ->join('categoria_maquinas AS CM',
                                'SCM.categoria_maquina_id','=','CM.id'
                              )
                        ->orderBy('tecnico_nombre','ASC')
                        ->orderBy('maquina_nombre','ASC')
                        ->orderBy('OT.tipo','ASC')
                        ->select(
                                  'OT.*',
                                  'M.nombre AS maquina_nombre',
                                  'M.identificador AS maquina_identificador',
                                  'M.marca AS maquina_marca',
                                  'SCM.nombre AS subcategoria_maquina_nombre',
                                  'CM.nombre AS categoria_maquina_nombre',
                                  'P.name AS tecnico_nombre',
                                  'P.lastname AS tecnico_apellido'
                                )

                        ->paginate(env('PAGINATION_PAGE_ROWS'));

        return \View::make('ordentrabajo.showall',
                              [
                                'ordenesTrabajo' => $ordenesTrabajo,
                                'isReportView'=>true
                              ]
                          );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $teatros = Empresa::orderBy('nombre','ASC')->get();
            
      return \View::make('reservas.add',
                            [
                              'teatros' => $teatros,
                              'isReportView'=>true
                            ]
                        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $ordenTrabajoId = 0;
        try {
              DB::beginTransaction();

              //ORDEN DE TRABAJO
              $maquina_id = $request->input('maquina_id');
              $tipo = $request->input('tipo');
              $fecha = $request->input('fecha');
              $tecnico_id = $request->input('tecnico_id');
              $usuario_id	= Auth::user()->id;
              $horas = $request->input('horas');
              $descripcion = $request->input('descripcion');
              $observaciones = $request->input('observaciones');
              $consecutivo = $request->input('consecutivo');
              $fileentryids = $request->input('fileentryid');
              $imagen_id = collect($fileentryids)->implode('|');

              //Log::info('IMAGEN ID: '.$imagen_id );


              $ordenTrabajo = new OrdenTrabajo();
              $ordenTrabajo-> maquina_id = $maquina_id;
              $ordenTrabajo-> tipo = $tipo;
              $ordenTrabajo-> fecha = $fecha;
              $ordenTrabajo-> tecnico_id = $tecnico_id;
              $ordenTrabajo-> usuario_id = $usuario_id;
              $ordenTrabajo-> horas = $horas;
              $ordenTrabajo-> descripcion = $descripcion;
              $ordenTrabajo-> observaciones = $observaciones;
              $ordenTrabajo-> consecutivo = $consecutivo;
              $ordenTrabajo-> imagen_id = $imagen_id;

              if ( $ordenTrabajo->save() ){ //IF GUARDADO ORDEN DE TRABAJO EXITOSO

                  $ordenTrabajoId = $ordenTrabajo->id;
                  //ORDEN DE TRABAJOS REPUESTOS
                  $repuestosId = $request->input('repuesto_id');
                  $origenesExterno = $request->input('origen_externo');
                  $cantidades = $request->input('cantidad');
                  $cantidadesExterno = $request->input('cantidad_externo');
                  $valores = $request->input('valor');
                  $valoresExterno = $request->input('valor_externo');
                  $proveedores = $request->input('proveedor');
                  $proveedoresExterno = $request->input('proveedor_externo');

                  for( $i = 0; $i < count($repuestosId); $i++ ){

                    $repuesto_id = $repuestosId[$i];

                    if( count($origenesExterno) > 0 && in_array($repuesto_id, $origenesExterno) ){

                      $origen = 'externo';
                      $cantidad = $cantidadesExterno[$i];
                      $valor_unitario = $valoresExterno[$i];
                      $proveedor = $proveedoresExterno[$i];

                    }else{ //ELSE STOCK LOCAL

                      $origen = 'stock';
                      $cantidad = $cantidades[$i];
                      $valor_unitario = $valores[$i];
                      $proveedor = $proveedores[$i];

                      $iR = InventarioRepuesto::where('repuesto_id',$repuesto_id)->first();
                      $cantidadInventario = $iR->cantidad;
                      $cantidadActual = $iR->cantidad;
                      $cantidadActual -= $cantidad;

                      //GUARDADO HISTÓRICO
                      $h = new HistoricoInventario();
                      $h->usuario_last_update_id   = $usuario_id;
                      $h->inventario_repuesto_id	 = $iR->id;
                      $h->repuesto_id	 = $iR->repuesto_id;
                      $h->cantidad	 = $iR->cantidad;
                      $h->valor	 = $iR->valor;
                      $h->tipo	 = $iR->tipo;
                      $h->proveedor	 = $iR->proveedor;
                      $h->imagen_id	 = $iR->imagen_id;
                      $h->detalles	 = $iR->detalles;
                      $h->observaciones	 = $iR->observaciones;
                      $h->empresa_id	 = $iR->empresa_id;
                      $h->tipo_evento	 = $iR->tipo_evento;
                      $h->cantidad_evento	 = $iR->cantidad_evento;
                      $h->orden_trabajo_id		 = $iR->orden_trabajo_id	;
                      $h->usuario_id	 = $iR->usuario_id;
                      $h->active	 = $iR->active;
                      $h->last_created_at	 = $iR->created_at;
                      $h->last_updated_at	 = $iR->updated_at;

                      if( !$h->save() ){ // IF GUARDADO HISTORICO FALLIDO

                        DB::rollBack();
                        Log::info('Error guardando el historico del inventario: '.print_r($request->all(),TRUE));
                        return redirect()->back()->withError('Error guardando la información del historico del inventario en la base de datos');

                      // IF GUARDADO HISTORICO FALLIDO
                      }else{ // ELSE GUARDADO HISTORICO EXITO

                        $iR->cantidad = $cantidadActual;
                        $iR->tipo_evento = 'egreso';
                        $iR->cantidad_evento = $cantidad;
                        $iR->orden_trabajo_id = $ordenTrabajo->id;
                        $iR->usuario_id = $usuario_id;

                        if( !$iR->save() ){ //IF ACTUALIZACIÓN INVENTARIO FALLIDO

                          DB::rollBack();
                          Log::info('Error actualizando el inventario: '.print_r($request->all(),TRUE));
                          return redirect()->back()->withError('Error actualizando la información del inventario en la base de datos');

                        //IF ACTUALIZACIÓN INVENTARIO FALLIDO
                        }
                      }// ELSE GUARDADO HISTORICO EXITO
                    }//ELSE DEL STOCK LOCAL

                    $ordenTrabajo
                      ->repuestos()
                        ->attach($repuesto_id,
                                  [
                                    'origen' => $origen,
                                    'cantidad' => $cantidad,
                                    'valor_unitario' => $valor_unitario,
                                    'proveedor' => $proveedor,
                                  ]
                                );
                  }

              //IF GUARDADO ORDEN DE TRABAJO EXITOSO
            }else{ //ELSE GUARDADO ORDEN DE TRABAJO FALLIDO
                DB::rollBack();
                Log::info('Error guardando Orden de Trabajo: '.print_r($request->all(),TRUE));
                return redirect()->back()->withError('Error guardando la información de la orden de trabajo en la base de datos');
              }//ELSE GUARDADO ORDEN DE TRABAJO FALLIDO

              DB::commit();
              $this->autosavePdf( $ordenTrabajoId );

              $request->session()->flash('flash_success_message', env('FLASH_SUCCESS_MESSAGE'));
              return redirect('ordenestrabajoshow/'.$ordenTrabajo->id);

        }catch(\Illuminate\Database\QueryException $ex){

          DB::rollBack();
          Log::info($ex->getMessage());
          return redirect()->back()->withError('Error guardando la información de la categoría en la base de datos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $ordenTrabajo = DB::table('orden_trabajos AS OT')
                        ->join('profiles AS P','OT.tecnico_id','=','P.user_id')
                        ->join('maquinas AS M','OT.maquina_id','=','M.id')
                        ->join('subcategoria_maquinas AS SCM',
                                'M.subcategoria_maquina_id','=','SCM.id'
                              )
                        ->join('categoria_maquinas AS CM',
                                'SCM.categoria_maquina_id','=','CM.id'
                              )
                        ->where('OT.id', $id)
                        ->select(
                                  'OT.*',
                                  'M.nombre AS maquina_nombre',
                                  'M.identificador AS maquina_identificador',
                                  'M.marca AS maquina_marca',
                                  'SCM.nombre AS subcategoria_maquina_nombre',
                                  'CM.nombre AS categoria_maquina_nombre',
                                  'P.name AS tecnico_nombre',
                                  'P.lastname AS tecnico_apellido'
                                )
                        ->first();

      $repuestos = DB::table('orden_trabajo_repuestos AS OTR')
                        ->join('repuestos AS R','OTR.repuesto_id','=','R.id')
                        ->join('categoria_repuestos AS CR',
                                'R.categoria_repuesto_id','=','CR.id'
                              )
                        ->where('OTR.orden_trabajo_id', $id)
                        ->orderBy('OTR.id','ASC')
                        ->select(
                                  'OTR.*',
                                  'R.referencia AS referencia',
                                  'R.marca AS marca',
                                  'CR.nombre AS categoria'
                                )
                        ->get();

      $images = array_filter(
                              explode('|',$ordenTrabajo->imagen_id),
                                function( $element ){
                                    return (strlen($element) > 0);
                              });

      return \View::make('ordentrabajo.show',
                            [
                              'ordenTrabajo' => $ordenTrabajo,
                              'repuestos' => $repuestos,
                              'images' => $images,
                              'isReportView'=>true
                            ]
                        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generatePdf($id)
    {
      $ordenTrabajo = DB::table('orden_trabajos AS OT')
                        ->join('profiles AS P','OT.tecnico_id','=','P.user_id')
                        ->join('maquinas AS M','OT.maquina_id','=','M.id')
                        ->join('subcategoria_maquinas AS SCM',
                                'M.subcategoria_maquina_id','=','SCM.id'
                              )
                        ->join('categoria_maquinas AS CM',
                                'SCM.categoria_maquina_id','=','CM.id'
                              )
                        ->where('OT.id', $id)
                        ->select(
                                  'OT.*',
                                  'M.nombre AS maquina_nombre',
                                  'M.identificador AS maquina_identificador',
                                  'M.marca AS maquina_marca',
                                  'SCM.nombre AS subcategoria_maquina_nombre',
                                  'CM.nombre AS categoria_maquina_nombre',
                                  'P.name AS tecnico_nombre',
                                  'P.lastname AS tecnico_apellido'
                                )
                        ->first();

      $repuestos = DB::table('orden_trabajo_repuestos AS OTR')
                        ->join('repuestos AS R','OTR.repuesto_id','=','R.id')
                        ->join('categoria_repuestos AS CR',
                                'R.categoria_repuesto_id','=','CR.id'
                              )
                        ->where('OTR.orden_trabajo_id', $id)
                        ->orderBy('OTR.id','ASC')
                        ->select(
                                  'OTR.*',
                                  'R.referencia AS referencia',
                                  'R.marca AS marca',
                                  'CR.nombre AS categoria'
                                )
                        ->get();

      $images = array_filter(
                              explode('|',$ordenTrabajo->imagen_id),
                                function( $element ){
                                    return (strlen($element) > 0);
                              });

      $view = \View::make('ordentrabajo.pdf',
                            [
                              'ordenTrabajo' => $ordenTrabajo,
                              'repuestos' => $repuestos,
                              'images' => $images,
                              'isReportView'=>true
                            ]
                        )->render();

      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('orden-trabajo-'.date('Y-m-d').'.pdf');
    }

    private function autosavePdf( $id ){
      $ordenTrabajo = DB::table('orden_trabajos AS OT')
                        ->join('profiles AS P','OT.tecnico_id','=','P.user_id')
                        ->join('maquinas AS M','OT.maquina_id','=','M.id')
                        ->join('subcategoria_maquinas AS SCM',
                                'M.subcategoria_maquina_id','=','SCM.id'
                              )
                        ->join('categoria_maquinas AS CM',
                                'SCM.categoria_maquina_id','=','CM.id'
                              )
                        ->where('OT.id', $id)
                        ->select(
                                  'OT.*',
                                  'M.nombre AS maquina_nombre',
                                  'M.identificador AS maquina_identificador',
                                  'M.marca AS maquina_marca',
                                  'SCM.nombre AS subcategoria_maquina_nombre',
                                  'CM.nombre AS categoria_maquina_nombre',
                                  'P.name AS tecnico_nombre',
                                  'P.lastname AS tecnico_apellido'
                                )
                        ->first();

      $repuestos = DB::table('orden_trabajo_repuestos AS OTR')
                        ->join('repuestos AS R','OTR.repuesto_id','=','R.id')
                        ->join('categoria_repuestos AS CR',
                                'R.categoria_repuesto_id','=','CR.id'
                              )
                        ->where('OTR.orden_trabajo_id', $id)
                        ->orderBy('OTR.id','ASC')
                        ->select(
                                  'OTR.*',
                                  'R.referencia AS referencia',
                                  'R.marca AS marca',
                                  'CR.nombre AS categoria'
                                )
                        ->get();

      $images = array_filter(
                              explode('|',$ordenTrabajo->imagen_id),
                                function( $element ){
                                    return (strlen($element) > 0);
                              });

      $view = \View::make('ordentrabajo.pdf',
                            [
                              'ordenTrabajo' => $ordenTrabajo,
                              'repuestos' => $repuestos,
                              'images' => $images,
                              'isReportView'=>true
                            ]
                        )->render();

      $pdf = \App::make('dompdf.wrapper');

      $pdfName = md5(env('DEFAULT_OT_FILE_FREFIX').$id).'.pdf';

      $pdf->loadHTML($view)->save(storage_path('app').'/'.$pdfName);
      $file = Storage::disk('local')->get($pdfName);
      $entry = new Fileentry();
  		$entry->mime = 'application/pdf';
  		$entry->original_filename = $pdfName;
  		$entry->filename = $pdfName;
  		$entry->filesize = Storage::size($pdfName);
      $entry->extension = 'pdf';
  		$entry->save();
    }

    public function getOrdenTrabajoNombreByOtId($id){

      return DB::table('orden_trabajos AS OT')
                    ->join('maquinas AS M','OT.maquina_id','=','M.id')
                    ->join(
                            'subcategoria_maquinas AS SCM','M.subcategoria_maquina_id',
                            '=',
                            'SCM.id')
                    ->join(
                            'categoria_maquinas AS CM',
                            'SCM.categoria_maquina_id',
                            '=',
                            'CM.id')
                    ->where('OT.id',$id)
                    ->select(
                              'OT.*',
                              'OT.consecutivo AS consecutivo',
                              'M.nombre AS maquina_nombre',
                              'SCM.nombre AS subcategoria_nombre',
                              'CM.nombre AS categoria_nombre'
                            )
                    ->first();
    }

    public function getPdfById($id){

      $datosNombre = $this->getOrdenTrabajoNombreByOtId($id);

      $tmpFileName = santize_filename($datosNombre->categoria_nombre);
      $tmpFileName .= santize_filename('-'.$datosNombre->subcategoria_nombre);
      $tmpFileName .= santize_filename('-'.$datosNombre->maquina_nombre);
      $tmpFileName .= santize_filename('-'.$datosNombre->consecutivo).'.pdf';

      $pdfName = md5(env('DEFAULT_OT_FILE_FREFIX').$id).'.pdf';
      $entry = Fileentry::where('filename', '=', $pdfName)->firstOrFail();
      $oldPath = storage_path('app').'/' .$pdfName;
      $newPath2 = public_path().'/tmp/'.$tmpFileName;
      $newPath = public_path().'/tmp/'.date('YmdHis').'.pdf';

      $headers = ['Content-Type' => $entry->mime];

      if (File::copy($oldPath , $newPath)) {
          $r = response()->download($newPath, $tmpFileName, $headers);
          return $r->deleteFileAfterSend(true);
      }
    }
}
