<?php

Route::get('/', function () {
   return redirect('home');
});

//Home Route with Controller

Route::get('home', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);

Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);
Route::get('profileadd', ['as' => 'profileadd', 'uses' => 'ProfileController@create']);
Route::post('profileadd', ['as' => 'profileadd', 'uses' => 'ProfileController@store']);
Route::get('profileedit', ['as' => 'profileedit', 'uses' => 'ProfileController@edit']);
Route::post('profileedit', ['as' => 'profileedit', 'uses' => 'ProfileController@update']);
Route::get('profileshow', ['as' => 'profileshow', 'uses' => 'ProfileController@show']);
Route::post('getbyuserid', ['as' => 'getbyuserid', 'uses' => 'ProfileController@getFullProfileDataByUserId']);

Route::get('profileeditaddsucursal/{id}', ['as' => 'profileeditaddsucursal', 'uses' => 'ProfileController@setSucursal','middleware' => 'role:admin']);
Route::post('profileeditaddsucursal', ['as' => 'profileeditaddsucursal', 'uses' => 'ProfileController@saveSucursal','middleware' => 'role:admin']);

Route::get('userroleshowall', ['as' => 'userroleshowall', 'uses' => 'UserRoleController@index','middleware' => 'role:admin']);
Route::get('userroleadd', ['as' => 'userroleadd', 'uses' => 'UserRoleController@create','middleware' => 'role:admin']);
Route::post('userroleadd', ['as' => 'userroleadd', 'uses' => 'UserRoleController@store','middleware' => 'role:admin']);
Route::get('userroleedit/{id}', ['as' => 'userroleedit', 'uses' => 'UserRoleController@edit','middleware' => 'role:admin']);
Route::post('userroleedit', ['as' => 'userroleedit', 'uses' => 'UserRoleController@update','middleware' => 'role:admin']);
Route::get('userroledelete', ['as' => 'userroledelete', 'uses' => 'UserRoleController@show','middleware' => 'role:admin']);

Route::get('fileentry', 'FileEntryController@index');
Route::get('fileentry/get/{filename}', ['as' => 'getentry', 'uses' => 'FileEntryController@get']);
Route::post('fileentry/add',['as' => 'addentry', 'uses' => 'FileEntryController@add']);
Route::post('fileentry/addajax',['as' => 'addentryajax', 'uses' => 'FileEntryController@addAjax']);
Route::get('fileentry/getajax/{id}',['as' => 'getentryajax', 'uses' => 'FileEntryController@getAjax']);
Route::get('fileentry/deleteajax/{id}',['as' => 'deleteentryajax', 'uses' => 'FileEntryController@deleteAjax']);
Route::get('fileentry/deleteajax/{id}',['as' => 'deleteentryajax', 'uses' => 'FileEntryController@deleteAjax']);
Route::get('images/{id}',['as' => 'images', 'uses' => 'FileEntryController@getImage']);
Route::get('imagehtml/{id}',['as' => 'imagehmtl', 'uses' => 'FileEntryController@get']);
Route::get('pdfs/{id}',['as' => 'images', 'uses' => 'FileEntryController@getPdf']);

/*
 * Empresas
 */

Route::get('empresaadd', ['as' => 'empresaadd', 'uses' => 'EmpresaController@create','middleware' => 'role:admin']);
Route::post('empresaadd', ['as' => 'empresaadd', 'uses' => 'EmpresaController@store','middleware' => 'role:admin']);
Route::get('empresaedit/{id}', ['as' => 'empresaedit', 'uses' => 'EmpresaController@edit','middleware' => 'role:admin']);
Route::post('empresaedit', ['as' => 'empresaedit', 'uses' => 'EmpresaController@update','middleware' => 'role:admin']);
Route::get('empresashowall', ['as' => 'empresashowall', 'uses' => 'EmpresaController@index','middleware' => 'role:admin']);
Route::get('empresadelete/{id}', ['as' => 'empresaedelete', 'uses' => 'EmpresaController@destroy','middleware' => 'role:admin']);

/*
 * Reservas
 */

Route::get('reserva/add', ['as' => 'reserva/add', 'uses' => 'ReservaController@create','middleware' => 'auth']);
Route::post('reserva/add', ['as' => 'reserva/add', 'uses' => 'ReservaController@store','middleware' => 'auth']);
Route::get('reserva/edit/{id}', ['as' => 'reserva/editpresaedit', 'uses' => 'ReservaController@edit','middleware' => 'role:admin|cliente']);
Route::post('reserva/edit', ['as' => 'reserva/edit', 'uses' => 'ReservaController@update','middleware' => 'role:admin|cliente']);
Route::get('reserva/all', ['as' => 'reserva/all', 'uses' => 'ReservaController@index','middleware' => 'role:admin|cliente']);
Route::get('reserva/delete/{id}', ['as' => 'reserva/delete', 'uses' => 'ReservaController@destroy','middleware' => 'role:admin|cliente']);

Route::get('getbutacasbyfuncionid/{id}', ['as' => 'getbutacasbyfuncionid', 'uses' => 'ReservaController@getButacasByFuncion','middleware' => 'auth']);
Route::get('getotrasbutacasbyfuncionid/{id}', ['as' => 'getotrasbutacasbyfuncionid', 'uses' => 'ReservaController@getOtrasButacasByFuncion','middleware' => 'auth']);

/*
 * Peliculas
 */

Route::get('peliculas/add', ['as' => 'peliculas/add', 'uses' => 'PeliculaController@create','middleware' => 'role:admin|cliente']);
Route::post('peliculas/add', ['as' => 'peliculas/add', 'uses' => 'PeliculaController@store','middleware' => 'role:admin|cliente']);
Route::get('peliculas/edit/{id}', ['as' => 'peliculas/editpresaedit', 'uses' => 'PeliculaController@edit','middleware' => 'role:admin|cliente']);
Route::post('peliculas/edit', ['as' => 'peliculas/edit', 'uses' => 'PeliculaController@update','middleware' => 'role:admin|cliente']);
Route::get('peliculas/all', ['as' => 'peliculas/all', 'uses' => 'PeliculaController@index','middleware' => 'role:admin|cliente']);
Route::get('peliculas/delete/{id}', ['as' => 'peliculas/delete', 'uses' => 'PeliculaController@destroy','middleware' => 'role:admin|cliente']);

Route::get('getpeliculasbyteatroid/{id}', ['as' => 'getpeliculasbyteatroid', 'uses' => 'PeliculaController@getByTeatro','middleware' => 'auth']);

/*
 * Salas
 */

Route::get('sala/add', ['as' => 'sala/add', 'uses' => 'SalaController@create','middleware' => 'role:admin|cliente']);
Route::post('sala/add', ['as' => 'sala/add', 'uses' => 'SalaController@store','middleware' => 'role:admin|cliente']);
Route::get('sala/edit/{id}', ['as' => 'sala/editpresaedit', 'uses' => 'SalaController@edit','middleware' => 'role:admin|cliente']);
Route::post('sala/edit', ['as' => 'sala/edit', 'uses' => 'SalaController@update','middleware' => 'role:admin|cliente']);
Route::get('sala/all', ['as' => 'sala/all', 'uses' => 'SalaController@index','middleware' => 'role:admin|cliente']);
Route::get('sala/delete/{id}', ['as' => 'sala/delete', 'uses' => 'SalaController@destroy','middleware' => 'role:admin|cliente']);

Route::get('getsalabyteatroid/{id}', ['as' => 'getsalabyteatroid', 'uses' => 'SalaController@getByTeatro','middleware' => 'auth']);

/*
 * Funciones/Horarios
 */

Route::get('funcion/add', ['as' => 'funcion/add', 'uses' => 'FuncionController@create','middleware' => 'role:admin|cliente']);
Route::post('funcion/add', ['as' => 'funcion/add', 'uses' => 'FuncionController@store','middleware' => 'role:admin|cliente']);
Route::get('funcion/edit/{id}', ['as' => 'funcion/editpresaedit', 'uses' => 'FuncionController@edit','middleware' => 'role:admin|cliente']);
Route::post('funcion/edit', ['as' => 'funcion/edit', 'uses' => 'FuncionController@update','middleware' => 'role:admin|cliente']);
Route::get('funcion/all', ['as' => 'funcion/all', 'uses' => 'FuncionController@index','middleware' => 'role:admin|cliente']);
Route::get('funcion/delete/{id}', ['as' => 'funcion/delete', 'uses' => 'FuncionController@destroy','middleware' => 'role:admin|cliente']);

Route::post('getfuncionesbypeliculaidsalaid', 
            [
                'as' => 'getfuncionesbypeliculaidsalaid', 
                'uses' => 'FuncionController@getByPeliculaSala',
                'middleware' => 'auth'
            ]
        );
